#based off of Burnin Up Group: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/makefile

.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        


# check the existence of check files
check: $(CFILES)

# remove temp files
clean:
	rm -f  *.tmp
	rm -rf __pycache__


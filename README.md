# FtblStatRat
Group Members (GitLab ID, EID):

Bo Deng: Bo-Deng, byd78

Nitin Jain: nitinjain442, Nj5649

Amer Jusupovic: amerj00, aj32328

Kelly Sun: ksun878, kps878

Erica Xu: ericaxu99, Jex57

Git SHA: 239e353937adf767bea58c1bac20015aa64acdab

Phase 1 Project Leader: Amer Jusupovic

Phase 2 Project Leader: Nitin Jain

Phase 3 Project Leader: Bo Deng

Phase 4 Project Leader: Kelly Sun

GitLab Pipelines: https://gitlab.com/cs373project/ftblstatrat/-/pipelines

Website: football-stats.me

Estimated completion time for each member:
- Bo Deng: 10/18/18/15
- Nitin Jain: 15/25/25/15
- Amer Jusupovic: 20/25/25/15
- Kelly Sun: 20/20/20/10
- Erica Xu: 20/15/15/10

Actual completion time for each member:
- Bo Deng: 12/18/15/11
- Nitin Jain: 25/30/27/10
- Amer Jusupovic: 30/25/23/15
- Kelly Sun: 25/25/12/9
- Erica Xu: 20/15/10/7

Comments: The YAML file, dockerfile, and makefile are based off of code from previous groups. The group and their repo link are noted in the corresponding file.

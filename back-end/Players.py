from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import time
import pandas as pd
import psycopg2


#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

class Player(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    player_name = db.Column(db.String())
    player_age = db.Column(db.Integer)
    player_height = db.Column(db.String())
    player_weight = db.Column(db.String())
    player_position = db.Column(db.String())
    player_teamName = db.Column(db.String())
    player_jerseyNumber = db.Column(db.String())
    player_hometown = db.Column(db.String())
    player_country = db.Column(db.String())
    player_games = db.Column(db.Integer)
    player_goals = db.Column(db.Integer)
    player_photo = db.Column(db.String())
    player_id_firebase = db.Column(db.Integer)
    #player_video = db.Column(db.String())
    
    def __init__(self, player_id_firebase = 0, player_name = "NaN", player_age = 0, player_height = "NaN", 
        player_weight = "NaN", player_position = "NaN", player_teamName = "NaN",
        player_jerseyNumber = "NaN", player_hometown = "NaN", player_country = "NaN", 
        player_games = 0, player_goals = 0, player_photo = "NaN"):
            self.player_name = player_name
            self.player_age = player_age
            self.player_height = player_height
            self.player_weight = player_weight
            self.player_position = player_position
            self.player_teamName = player_teamName
            self.player_jerseyNumber = player_jerseyNumber
            self.player_hometown = player_hometown
            self.player_country = player_country
            self.player_games = player_games
            self.player_goals = player_goals
            self.player_photo = player_photo
            self.player_id_firebase = player_id_firebase
            #self.player_video = player_video

db.create_all()

cred = credentials.Certificate('./creds.json')
firebase_admin.initialize_app(cred)

data = firestore.client()

data_ref = data.collection(u'players')
docs = data_ref.stream()

player_list = []

#for item in docs:
#    data_dict = item.to_dict()
#    print(data_dict)


# DEVELOPER_KEY = os.getenv("YOUTUBE_API_KEY")
# YOUTUBE_API_SERVICE_NAME = 'youtube'
# YOUTUBE_API_VERSION = 'v3'
# os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./creds.json"

# def youtube_search(query, maxResults):
#     youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
#         developerKey=DEVELOPER_KEY)

#     # Call the search.list method to retrieve results matching the specified
#     # query term.
#     search_response = youtube.search().list(
#         q=query,
#         part='id,snippet',
#         maxResults=maxResults
#     ).execute()

#     for search_result in search_response.get('items', []):
#         if search_result['id']['kind'] == 'youtube#video':
#             return (f'https://www.youtube.com/watch?v={search_result["id"]["videoId"]}')

#     return ''

for item in docs:
    item = item.to_dict()
    # try:
    #     video = youtube_search(f'{item["player"]["name"]} highlights', 5)
    #     #print(player_video)
    # except HttpError as e:
    #     print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
    
    new_player = Player(
        player_name=item["player"]["name"],
        player_age=item["player"]["age"],
        player_height=item["player"]["height"],
        player_weight=item["player"]["weight"],
        player_position=item["statistics"][0]["games"]["position"],
        player_teamName=item["statistics"][0]["team"]["name"],
        player_teamID_firebase=item["statistics"][0]["team"]["id"],
        player_jerseyNumber=item["statistics"][0]["games"]["number"],
        player_hometown=item["player"]["birth"]["place"],
        player_country=item["player"]["birth"]["country"],
        player_games=item["statistics"][0]["games"]["appearences"],
        player_goals=item["statistics"][0]["goals"]["total"],
        player_photo=item["player"]["photo"],
        player_id_firebase=item["player"]["id"]
        #player_video=video
    )
    player_list.append(new_player)

db.session.add_all(player_list)
db.session.commit()

# based off of burnin group: https://gitlab.com/caitlinlien/cs373-sustainability/-/blob/master/backend/main.py

from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os
from flask_cors import CORS

from models import *

from TeamSearchSortFilter import *
from MatchSearchSortFilter import *
from PlayerSearchSortFilter import *

# Initialize app
app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# db
load_dotenv()

app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")

# db = SQLAlchemy(app)
ma = Marshmallow(app)
db.init_app(app)


class TeamSchema(ma.Schema):
    team_id = fields.Int(required=True)
    team_name = fields.Str(required=False)
    team_country = fields.Str(required=False)
    team_founded = fields.Int(required=False)
    team_logoLink = fields.Str(required=False)
    team_league = fields.Str(required=False)
    team_record = fields.Str(required=False)
    team_goals = fields.Int(required=False)
    team_formation = fields.Str(required=False)
    team_venueCity = fields.Str(required=False)
    team_venueSurface = fields.Str(required=False)
    team_venueCapacity = fields.Int(required=False)
    team_venueName = fields.Str(required=False)
    team_form = fields.Str(required=False)
    team_goalsForAverage = fields.Str(required=False)
    team_goalsAgainstAverage = fields.Str(required=False)
    team_penaltyPercentage = fields.Str(required=False)
    team_video = fields.Str(required=False)


class MatchSchema(ma.Schema):
    match_id = fields.Int(required=True)
    match_score = fields.Str(required=False)
    match_date = fields.Str(required=False)
    match_prediction = fields.Str(required=False)
    match_league = fields.Str(required=False)
    match_video = fields.Str(required=False)
    match_location = fields.Str(required=False)
    match_venue_name = fields.Str(required=False)
    match_home_name = fields.Str(required=False)
    match_home_players = fields.Str(required=False)  # maybe
    match_home_shotsOnGoal = fields.Int(required=False)
    match_home_shotsOffGoal = fields.Int(required=False)
    match_home_cornerKicks = fields.Int(required=False)
    match_home_posessionPercentage = fields.Str(required=False)
    match_home_passCompletion = fields.Str(required=False)
    match_home_yellowCards = fields.Int(required=False)
    match_home_redCards = fields.Int(required=False)
    match_away_name = fields.Str(required=False)
    match_away_players = fields.Str(required=False)  # maybe
    match_away_shotsOnGoal = fields.Int(required=False)
    match_away_shotsOffGoal = fields.Int(required=False)
    match_away_cornerKicks = fields.Int(required=False)
    match_away_posessionPercentage = fields.Str(required=False)
    match_away_passCompletion = fields.Str(required=False)
    match_away_yellowCards = fields.Int(required=False)
    match_away_redCards = fields.Int(required=False)


class PlayerSchema(ma.Schema):
    player_id = fields.Int(required=True)
    player_name = fields.Str(required=False)
    player_age = fields.Int(required=False)
    player_height = fields.Str(required=False)
    player_weight = fields.Str(required=False)
    player_position = fields.Str(required=False)
    player_teamName = fields.Str(required=False)
    player_jerseyNumber = fields.Str(required=False)
    player_hometown = fields.Str(required=False)
    player_country = fields.Str(required=False)
    player_games = fields.Int(required=False)
    player_goals = fields.Int(required=False)
    player_photo = fields.Str(required=False)


class MatchplayerSchema(ma.Schema):
    match_id = fields.Int(required=True)
    players = fields.Str(required=False)


class MatchteamSchema(ma.Schema):
    match_id = fields.Int(required=True)
    team_home_id = fields.Str(required=False)
    team_away_id = fields.Str(required=False)


class PlayerteamSchema(ma.Schema):
    player_id = fields.Int(required=True)
    team_id = fields.Str(required=False)


team_schema = TeamSchema()
teams_schema = TeamSchema(many=True)

match_schema = MatchSchema()
matches_schema = MatchSchema(many=True)

player_schema = PlayerSchema()
players_schema = PlayerSchema(many=True)

matchplayer_schema = MatchplayerSchema()
matchplayers_schema = MatchplayerSchema(many=True)

matchteam_schema = MatchteamSchema()
matchteams_schema = MatchteamSchema(many=True)

playerteam_schema = PlayerteamSchema()
playerteams_schema = PlayerteamSchema(many=True)


@app.route("/")
def get_mainPage():
    return jsonify({"Welcome to the api of FtblStatRat": ":)"})


@app.route("/teams", methods=["GET"])
def get_AllTeams():
    queries = request.args.to_dict(flat=False)
    team_query = db.session.query(Teams)

    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    q = get_query("q", queries)
    if q:
        team_query = search_teams(q,team_query)
        
    team_query = filter_teams(team_query, queries)

    sort = get_query("sort", queries)
    team_query = sort_teams(sort, team_query)

    count = team_query.count()

    if page != -1:
        per_page = (
            int(get_query("perPage", queries).pop())
            if get_query("perPage", queries)
            else 20
        )
        teams = team_query.paginate(page=page, per_page=per_page)
        result = teams_schema.dump(teams.items)
    else:
        result = teams_schema.dump(team_query)

    return jsonify({"teams": result, "count":count})


@app.route("/teams/id=<id>", methods=["GET"])
def get_team_id(id):
    team = Teams.query.get(id)
    if team is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return team_schema.jsonify(team)


@app.route("/players", methods=["GET"])
def get_AllPlayers():
    queries = request.args.to_dict(flat=False)
    player_query = db.session.query(Player)

    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    q = get_query("q", queries)
    if q:
        player_query = search_players(q, player_query)

    player_query = filter_players(player_query, queries)


    sort = get_query("sort", queries)
    player_query = sort_players(sort, player_query)

    count = player_query.count()

    if page != -1:
        per_page = (
            int(get_query("perPage", queries).pop())
            if get_query("perPage", queries)
            else 100
        ) 
        players = player_query.paginate(page=page, per_page=per_page)
        result = players_schema.dump(players.items)
    else:
        result = players_schema.dump(player_query)

    return jsonify({"players": result, "count": count})


@app.route("/players/id=<id>", methods=["GET"])
def get_player_id(id):
    player = Player.query.get(id)
    if player is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return player_schema.jsonify(player)


@app.route("/matches", methods=["GET"])
def get_AllMatches():
    queries = request.args.to_dict(flat=False)
    match_query = db.session.query(Match)

    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    q = get_query("q", queries)
    if q:
        match_query = search_matches(q, match_query)

    match_query = filter_matches(match_query, queries)

    sort = get_query("sort", queries)
    match_query = sort_matches(sort, match_query)

    count = match_query.count()

    if page != -1:
        per_page = (
            int(get_query("perPage", queries).pop())
            if get_query("perPage", queries)
            else 20
        ) 
        matches = match_query.paginate(page=page, per_page=per_page)
        result = matches_schema.dump(matches.items)
    else:
        result = matches_schema.dump(match_query)
    return jsonify({"matches": result, "count": count})


@app.route("/matches/id=<id>", methods=["GET"])
def get_match_id(id):
    match = Match.query.get(id)
    if match is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return match_schema.jsonify(match)


@app.route("/matches/players", methods=["GET"])
def get_matchplayers():
    allMatchPlayers = Matchplayer.query.all()
    result = matchplayers_schema.dump(allMatchPlayers)
    return jsonify({"matchplayer ids": result})


@app.route("/players/matchid=<id>", methods=["GET"])
def get_matchplayer_id(id):
    match = Matchplayer.query.get(id)
    if match is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return matchplayer_schema.jsonify(match)


@app.route("/matches/teams", methods=["GET"])
def get_matchteams():
    allMatchTeams = Matchteam.query.all()
    result = matchteams_schema.dump(allMatchTeams)
    return jsonify({"matchteam ids": result})


@app.route("/teams/matchid=<id>", methods=["GET"])
def get_matchteam_id(id):
    match = Matchteam.query.get(id)
    if match is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return matchteam_schema.jsonify(match)


@app.route("/players/teams", methods=["GET"])
def get_playerteams():
    allPlayerTeams = Playerteam.query.all()
    result = playerteams_schema.dump(allPlayerTeams)
    return jsonify({"playerteam ids": result})


@app.route("/teams/playerid=<id>", methods=["GET"])
def get_playerteam_id(id):
    player = Playerteam.query.get(id)
    if player is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return playerteam_schema.jsonify(player)


if __name__ == "__main__":
    app.run(debug=True, port=5000, host="0.0.0.0") 

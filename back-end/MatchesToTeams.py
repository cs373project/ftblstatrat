from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import time
import http

#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

class MatchTeam(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    team_home_id = db.Column(db.Integer)
    team_away_id = db.Column(db.Integer)

    def __init__(self, match_id = 0, team_home_id = 0, team_away_id = 0):
        self.match_id = match_id
        self.team_home_id = team_home_id
        self.team_away_id = team_away_id

class MatchNew(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    match_score = db.Column(db.String())
    match_prediction = db.Column(db.String())
    match_location = db.Column(db.String())
    match_video = db.Column(db.String())
    match_home_name = db.Column(db.String())
    match_home_players = db.Column(db.String())
    match_home_shotsOnGoal = db.Column(db.Integer)
    match_home_shotsOffGoal = db.Column(db.Integer)
    match_home_cornerKicks = db.Column(db.Integer)
    match_home_posessionPercentage = db.Column(db.String())
    match_home_passCompletion = db.Column(db.String())
    match_home_yellowCards = db.Column(db.Integer)
    match_home_redCards = db.Column(db.Integer)
    match_away_name = db.Column(db.String())
    match_away_players = db.Column(db.String())
    match_away_shotsOnGoal = db.Column(db.Integer)
    match_away_shotsOffGoal = db.Column(db.Integer)
    match_away_cornerKicks = db.Column(db.Integer)
    match_away_posessionPercentage = db.Column(db.String())
    match_away_passCompletion = db.Column(db.String())
    match_away_yellowCards = db.Column(db.Integer)
    match_away_redCards = db.Column(db.Integer)

class Teams(db.Model):
    team_id = db.Column(db.Integer, primary_key=True)
    team_name = db.Column(db.String())
    team_country = db.Column(db.String())
    team_founded = db.Column(db.Integer)
    team_logoLink = db.Column(db.String())
    team_league = db.Column(db.String())
    team_record = db.Column(db.String())
    team_goals = db.Column(db.Integer)
    team_formation = db.Column(db.String())
    team_venueCity = db.Column(db.String())
    team_venueSurface = db.Column(db.String())
    
db.create_all()

match_team_list = []

for match, home_name, away_name in db.session.query(MatchNew.match_id, MatchNew.match_home_name, MatchNew.match_away_name).all():
    home_id = 0
    away_id = 0
    
    home = db.session.query(Teams).filter_by(team_name=home_name).first()
    if home is not None:
        home_id = home.team_id
    away = db.session.query(Teams).filter_by(team_name=away_name).first()
    print(away)
    if away is not None:
        away_id = away.team_id
    
    new_match_team = MatchTeam(
        match_id = match,
        team_home_id = home_id,
        team_away_id = away_id
    )
    match_team_list.append(new_match_team)

db.session.add_all(match_team_list)
db.session.commit()

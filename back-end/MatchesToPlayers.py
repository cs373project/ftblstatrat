from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

# holds the players in a match
class MatchPlayer(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    players = db.Column(db.String)

    def __init__(self, match_id = 0, players = "NaN"):
        self.match_id = match_id
        self.players = players
        
class MatchNew(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    match_score = db.Column(db.String())
    match_prediction = db.Column(db.String())
    match_location = db.Column(db.String())
    match_video = db.Column(db.String())
    match_home_name = db.Column(db.String())
    match_home_players = db.Column(db.String())
    match_home_shotsOnGoal = db.Column(db.Integer)
    match_home_shotsOffGoal = db.Column(db.Integer)
    match_home_cornerKicks = db.Column(db.Integer)
    match_home_posessionPercentage = db.Column(db.String())
    match_home_passCompletion = db.Column(db.String())
    match_home_yellowCards = db.Column(db.Integer)
    match_home_redCards = db.Column(db.Integer)
    match_away_name = db.Column(db.String())
    match_away_players = db.Column(db.String())
    match_away_shotsOnGoal = db.Column(db.Integer)
    match_away_shotsOffGoal = db.Column(db.Integer)
    match_away_cornerKicks = db.Column(db.Integer)
    match_away_posessionPercentage = db.Column(db.String())
    match_away_passCompletion = db.Column(db.String())
    match_away_yellowCards = db.Column(db.Integer)
    match_away_redCards = db.Column(db.Integer)

class Player(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    player_name = db.Column(db.String())
    player_age = db.Column(db.Integer)
    player_height = db.Column(db.String())
    player_weight = db.Column(db.String())
    player_position = db.Column(db.String())
    player_teamName = db.Column(db.String())
    player_jerseyNumber = db.Column(db.String())
    player_hometown = db.Column(db.String())
    player_country = db.Column(db.String())
    player_games = db.Column(db.Integer)
    player_goals = db.Column(db.Integer)
    player_photo = db.Column(db.String())
    player_id_firebase = db.Column(db.Integer)

class Teams(db.Model):
    team_id = db.Column(db.Integer, primary_key=True)
    team_name = db.Column(db.String())
    team_country = db.Column(db.String())
    team_founded = db.Column(db.Integer)
    team_logoLink = db.Column(db.String())
    team_league = db.Column(db.String())
    team_record = db.Column(db.String())
    team_goals = db.Column(db.Integer)
    team_formation = db.Column(db.String())
    team_venueCity = db.Column(db.String())
    team_venueSurface = db.Column(db.String())

class PlayerTeam(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer)

class MatchTeam(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    team_home_id = db.Column(db.Integer)
    team_away_id = db.Column(db.Integer)

db.create_all()

match_player_list = []

for match, home, away in db.session.query(MatchNew.match_id, MatchNew.match_home_players, MatchNew.match_away_players):
    # list of players from home team
    home_players = home.split(" ")
    away_players = away.split(" ")
    #print(home_players)
    
    string = " "
    for i in range((len(home_players)-1)):    
        player = db.session.query(Player).filter_by(player_id_firebase=home_players[i]).first()
        if player is not None:
            string += str(player.player_id) + " "
    
    for i in range((len(away_players)-1)):
        player = db.session.query(Player).filter_by(player_id_firebase=home_players[i]).first()
        if player is not None:
            string += str(player.player_id) + " "
            
        new_match_player = MatchPlayer(
        match_id = match,
        players = string
    )
    match_player_list.append(new_match_player)

db.session.add_all(match_player_list)
db.session.commit()

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Teams(db.Model):
    team_id = db.Column(db.Integer, primary_key=True)
    team_name = db.Column(db.String())
    team_country = db.Column(db.String())
    team_founded = db.Column(db.Integer)
    team_logoLink = db.Column(db.String())
    team_league = db.Column(db.String())
    team_record = db.Column(db.String())
    team_goals = db.Column(db.Integer)
    team_formation = db.Column(db.String())
    team_venueCity = db.Column(db.String())
    team_venueSurface = db.Column(db.String())
    team_venueCapacity = db.Column(db.Integer)
    team_venueName = db.Column(db.String())
    team_form = db.Column(db.String())
    team_goalsForAverage = db.Column(db.String())
    team_goalsAgainstAverage = db.Column(db.String())
    team_penaltyPercentage = db.Column(db.String())
    team_video = db.Column(db.String())


class Match(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    match_score = db.Column(db.String())
    match_date = db.Column(db.String())
    match_prediction = db.Column(db.String())
    match_league = db.Column(db.String())
    match_video = db.Column(db.String())
    match_location = db.Column(db.String())
    match_venue_name = db.Column(db.String())
    match_home_name = db.Column(db.String())
    match_home_players = db.Column(db.String())  # maybe
    match_home_shotsOnGoal = db.Column(db.Integer)
    match_home_shotsOffGoal = db.Column(db.Integer)
    match_home_cornerKicks = db.Column(db.Integer)
    match_home_posessionPercentage = db.Column(db.String())
    match_home_passCompletion = db.Column(db.String())
    match_home_yellowCards = db.Column(db.Integer)
    match_home_redCards = db.Column(db.Integer)
    match_away_name = db.Column(db.String())
    match_away_players = db.Column(db.String())  # maybe
    match_away_shotsOnGoal = db.Column(db.Integer)
    match_away_shotsOffGoal = db.Column(db.Integer)
    match_away_cornerKicks = db.Column(db.Integer)
    match_away_posessionPercentage = db.Column(db.String())
    match_away_passCompletion = db.Column(db.String())
    match_away_yellowCards = db.Column(db.Integer)
    match_away_redCards = db.Column(db.Integer)


class Player(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    player_name = db.Column(db.String())
    player_age = db.Column(db.Integer)
    player_height = db.Column(db.String())
    player_weight = db.Column(db.String())
    player_position = db.Column(db.String())
    player_teamName = db.Column(db.String())
    player_jerseyNumber = db.Column(db.String())
    player_hometown = db.Column(db.String())
    player_country = db.Column(db.String())
    player_games = db.Column(db.Integer)
    player_goals = db.Column(db.Integer)
    player_photo = db.Column(db.String())


class Matchplayer(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    players = db.Column(db.String())


class Matchteam(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    team_home_id = db.Column(db.String())
    team_away_id = db.Column(db.String())


class Playerteam(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.String())

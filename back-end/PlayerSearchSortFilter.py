#based on TexasVotes group: https://github.com/forbesye/texasvotes/blob/master/back-end/District.py
from models import (
    Teams,
    Match ,
    Player,
    db
)

from sqlalchemy import and_, or_, func

def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None


def filter_player_by(player_query, filtering, what):
    if filtering == 'country':
        player_query = player_query.filter(Player.player_country.in_(what))
    elif filtering == 'age':
        # player_query = player_query.filter(Player.player_age.in_(what))
        player_query = player_query.filter(
            and_(
                Player.player_age >= what[0],
                Player.player_age <= what[1],
            )
        )

    elif filtering == 'position':
        player_query = player_query.filter(Player.player_position.in_(what))
 
    return player_query

def filter_players(player_query, queries):
    country = get_query('country', queries)
    ageRange = get_query('ageRange', queries)
    position = get_query('position', queries)

    if country != None:
        player_query = filter_player_by(player_query, 'country', country)
    
    if ageRange != None:
        ageRange = ageRange[0]
        minAge = 0
        maxAge = 100
        if len(ageRange.split("-")) < 2:
            minAge = int(ageRange)
        else:
            minAge, maxAge = ageRange.split("-")
        player_query = filter_player_by(player_query, "age", [minAge, maxAge])

    
    if position != None:
        player_query = filter_player_by(player_query, 'position', position)
    
    
    return player_query

def sort_player_by(sorting, player_query, desc):
    playerSortBy = None
    if sorting == "goals":
        playerSortBy = Player.player_goals
    elif sorting == "gamesplayed":
        playerSortBy = Player.player_games
    else:
        return player_query
    
    if desc:
        return player_query.order_by(playerSortBy.desc())
    else:
        return player_query.order_by(playerSortBy)

def sort_players(sort, player_query):
    if sort == None:
        return player_query
    else:
        sort = sort[0]
    
    sort = sort.split("-")

    if len(sort) > 1:
        return sort_player_by(sort[1], player_query, True)
    else:
        return sort_player_by(sort[0], player_query, False)


def search_players(q, player_query):
    if not q:
        return player_query
    else:
        q = q[0].strip()
    
    terms = q.split()
    terms = [w.lower() for w in terms]

    searches = []
    for s in terms:
        searches.append(Player.player_name.ilike("%{}%".format(s)))
    
    player_query = player_query.filter(or_(*tuple(searches)))
    return player_query
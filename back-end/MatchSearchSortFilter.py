#based on TexasVotes group: https://github.com/forbesye/texasvotes/blob/master/back-end/District.py
from models import (
    Teams,
    Match,
    Player,
    db
)

from sqlalchemy import and_, or_, func

def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None

def filter_match_by(match_query, filtering, what):

    if filtering == 'hometeam':
        match_query = match_query.filter(Match.match_home_name.in_(what))
    elif filtering == 'awayteam':
        match_query = match_query.filter(Match.match_away_name.in_(what))
    elif filtering == 'league':
        match_query = match_query.filter(Match.match_league.in_(what))
    
    return match_query

def filter_matches(match_query, queries):
    hometeam = get_query('hometeam', queries)
    awayteam = get_query('awayteam', queries)
    league = get_query('league', queries)
    
    if hometeam != None:
        match_query = filter_match_by(match_query, 'hometeam', hometeam)
    
    if awayteam != None:
        match_query = filter_match_by(match_query, 'awayteam', awayteam)
    
    if league != None:
        match_query = filter_match_by(match_query, 'league', league)
    
    return match_query

def sort_match_by(sorting, match_query, desc):
    matchSortBy = None
    if sorting == "stadium":
        matchSortBy = Match.match_venue_name
    elif sorting == "city":
        matchSortBy = Match.match_location
    else:
        return match_query
    
    if desc:
        return match_query.order_by(matchSortBy.desc())
    else:
        return match_query.order_by(matchSortBy)


def sort_matches(sort, match_query):
    if sort == None:
        return match_query
    else:
        sort = sort[0]
    
    sort = sort.split("-")

    if len(sort) > 1:
        return sort_match_by(sort[1], match_query, True)
    else:
        return sort_match_by(sort[0], match_query, False)
    
def search_matches(q,match_query):
    if not q:
        return match_query
    else:
        q = q[0].strip()
    
    terms = q.split()
    terms = [w.lower() for w in terms]

    searches = []
    for s in terms:
        searches.append(Match.match_away_name.ilike("%{}%".format(s)))
        searches.append(Match.match_home_name.ilike("%{}%".format(s)))

    
    match_query = match_query.filter(or_(*tuple(searches)))
    return match_query

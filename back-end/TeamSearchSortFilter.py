#based on TexasVotes group: https://github.com/forbesye/texasvotes/blob/master/back-end/District.py
from models import (
    Teams,
    Match,
    Player,
    db
)

from sqlalchemy import and_, or_, func

def get_query(name, queries):
    try:
        return queries[name]
    except KeyError:
        return None

def filter_team_by(team_query, filtering, what):
    if filtering == 'yearfounded':
         team_query = team_query.filter(
            and_(
                Teams.team_founded >= what[0],
                Teams.team_founded <= what[1],
            )
        )

    elif filtering == 'country':
        team_query = team_query.filter(Teams.team_country.in_(what))
    elif filtering == 'league':
        team_query = team_query.filter(Teams.team_league.in_(what))
    # elif filtering == 'name':
    #     team_query = team_query.filter(Teams.team_name.in_(what))
    # elif filtering == 'streak':
    #     pass
    
    return team_query

def filter_teams(team_query, queries):
    yearRange = get_query('yearRange', queries)
    country = get_query('country', queries)
    league= get_query('league', queries)

    if yearRange != None:
        yearRange = yearRange[0]
        minYear = 1600
        maxYear = 2022
        if len(yearRange.split("-")) < 2:
            minYear = int(yearRange)
        else :
            minYear, maxYear = yearRange.split("-")
        team_query = filter_team_by(team_query, 'yearfounded', [minYear, maxYear])
    
    if country != None:
        team_query = filter_team_by(team_query, 'country', country)
    
    if league != None:
        team_query = filter_team_by(team_query, 'league', league)
    
    return team_query

def sort_team_by(sorting, team_query, desc):
    teamSortBy = None
    if sorting == "goals":
        teamSortBy = Teams.team_goals
    elif sorting == "penaltypercent":
        teamSortBy = Teams.team_penaltyPercentage
    else:
        return team_query

    if desc:
        return team_query.order_by(teamSortBy.desc())
    else:
        return team_query.order_by(teamSortBy)


def sort_teams(sort, team_query):
    if sort == None:
        return team_query
    else:
        sort = sort[0]
    
    sort = sort.split("-")

    if len(sort) > 1:
        return sort_team_by(sort[1], team_query, True)
    else:
        return sort_team_by(sort[0], team_query, False)

def search_teams(q, team_query):
    if not q:
        return team_query
    else:
        q = q[0].strip()
    
    terms = q.split()
    terms = [w.lower() for w in terms]

    searches = []
    for s in terms:
        searches.append(Teams.team_name.ilike("%{}%".format(s)))
    
    team_query = team_query.filter(or_(*tuple(searches)))
    return team_query
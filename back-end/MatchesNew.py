from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import time
import http
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

class MatchNew(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    match_score = db.Column(db.String())
    match_date = db.Column(db.String()) # new
    match_prediction = db.Column(db.String())
    match_league = db.Column(db.String()) # new
    match_video = db.Column(db.String())
    match_location = db.Column(db.String())
    match_venue_name = db.Column(db.String()) # new
    match_home_name = db.Column(db.String())
    match_home_players = db.Column(db.String())
    match_home_shotsOnGoal = db.Column(db.Integer)
    match_home_shotsOffGoal = db.Column(db.Integer)
    match_home_cornerKicks = db.Column(db.Integer)
    match_home_posessionPercentage = db.Column(db.String())
    match_home_passCompletion = db.Column(db.String())
    match_home_yellowCards = db.Column(db.Integer)
    match_home_redCards = db.Column(db.Integer)
    match_away_name = db.Column(db.String())
    match_away_players = db.Column(db.String())
    match_away_shotsOnGoal = db.Column(db.Integer)
    match_away_shotsOffGoal = db.Column(db.Integer)
    match_away_cornerKicks = db.Column(db.Integer)
    match_away_posessionPercentage = db.Column(db.String())
    match_away_passCompletion = db.Column(db.String())
    match_away_yellowCards = db.Column(db.Integer)
    match_away_redCards = db.Column(db.Integer)

    def __init__(self, match_score = "NaN", match_date = "NaN", match_prediction = "NaN", 
        match_league = "NaN", match_video = "NaN", match_location = "NaN", match_venue_name = "NaN", 
        match_home_name = "NaN", match_home_players = "NaN", 
        match_home_shotsOnGoal = 0, match_home_shotsOffGoal = 0, match_home_cornerKicks = 0, 
        match_home_posessionPercentage = "NaN", match_home_passCompletion = "NaN", match_home_yellowCards = 0, 
        match_home_redCards = 0, match_away_name = "NaN", match_away_players = "NaN", 
        match_away_shotsOnGoal = 0, match_away_shotsOffGoal = 0, match_away_cornerKicks = 0, 
        match_away_posessionPercentage = "NaN", match_away_passCompletion = "NaN", match_away_yellowCards = 0, 
        match_away_redCards = 0):
            self.match_score = match_score
            self.match_date = match_date
            self.match_prediction = match_prediction
            self.match_league = match_league
            self.match_video = match_video
            self.match_location = match_location
            self.match_venue_name = match_venue_name
            self.match_home_name = match_home_name
            self.match_home_players = match_home_players
            self.match_home_shotsOnGoal = match_home_shotsOnGoal
            self.match_home_shotsOffGoal = match_home_shotsOffGoal
            self.match_home_cornerKicks = match_home_cornerKicks
            self.match_home_posessionPercentage = match_home_posessionPercentage
            self.match_home_passCompletion = match_home_passCompletion
            self.match_home_yellowCards = match_home_yellowCards
            self.match_home_redCards = match_home_redCards
            self.match_away_name = match_away_name
            self.match_away_players = match_away_players
            self.match_away_shotsOnGoal = match_away_shotsOnGoal
            self.match_away_shotsOffGoal = match_away_shotsOffGoal
            self.match_away_cornerKicks = match_away_cornerKicks
            self.match_away_posessionPercentage = match_away_posessionPercentage
            self.match_away_passCompletion = match_away_passCompletion
            self.match_away_yellowCards = match_away_yellowCards
            self.match_away_redCards = match_away_redCards

class Match(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    match_score = db.Column(db.String())
    match_prediction = db.Column(db.String())
    match_location = db.Column(db.String())
    match_video = db.Column(db.String())
    match_home_name = db.Column(db.String())
    match_home_players = db.Column(db.String())
    match_home_shotsOnGoal = db.Column(db.Integer)
    match_home_shotsOffGoal = db.Column(db.Integer)
    match_home_cornerKicks = db.Column(db.Integer)
    match_home_posessionPercentage = db.Column(db.String())
    match_home_passCompletion = db.Column(db.String())
    match_home_yellowCards = db.Column(db.Integer)
    match_home_redCards = db.Column(db.Integer)
    match_away_name = db.Column(db.String())
    match_away_players = db.Column(db.String())
    match_away_shotsOnGoal = db.Column(db.Integer)
    match_away_shotsOffGoal = db.Column(db.Integer)
    match_away_cornerKicks = db.Column(db.Integer)
    match_away_posessionPercentage = db.Column(db.String())
    match_away_passCompletion = db.Column(db.String())
    match_away_yellowCards = db.Column(db.Integer)
    match_away_redCards = db.Column(db.Integer)

db.create_all()

cred = credentials.Certificate('./creds.json')
firebase_admin.initialize_app(cred)

fire_db = firestore.client()

data_ref = fire_db.collection(u'matches')
match_data = data_ref.stream()

#match_stats_ref = fire_db.collection(u'matchstats')
#match_stats = match_stats_ref.stream()

#conn = http.client.HTTPSConnection("v3.football.api-sports.io")

match_new_list = []

n = 0
for item in match_data:
    item = item.to_dict()

    if n > 205:
        break
    elif n >= 20:
        away_name=item["teams"]["away"]["name"]
        prediction=item["fixture"]["prediction"]
        venue_name=item["fixture"]["venue"]["name"]
        date=item["fixture"]["date"]
        league=item["league"]["name"]
        
        match = db.session.query(Match).filter_by(match_id=(n+1)).first()
        
        match_new = MatchNew(
            match_score=match.match_score, 
            match_prediction = prediction,
            match_location=match.match_location, 
            match_venue_name=venue_name,
            match_date=date,
            match_league=league,
            match_video=match.match_video,
            match_home_name=match.match_home_name, 
            match_home_players=match.match_home_players, 
            match_home_shotsOnGoal=match.match_home_shotsOnGoal, 
            match_home_shotsOffGoal=match.match_home_shotsOffGoal, 
            match_home_cornerKicks=match.match_home_cornerKicks, 
            match_home_posessionPercentage=match.match_home_posessionPercentage, 
            match_home_passCompletion=match.match_home_passCompletion, 
            match_home_yellowCards=match.match_home_yellowCards, 
            match_home_redCards=match.match_home_redCards,
            match_away_name=away_name,
            match_away_players=match.match_away_players,
            match_away_shotsOnGoal=match.match_away_shotsOnGoal,
            match_away_shotsOffGoal=match.match_away_shotsOffGoal,
            match_away_cornerKicks=match.match_away_cornerKicks, 
            match_away_posessionPercentage=match.match_away_posessionPercentage, 
            match_away_passCompletion=match.match_away_passCompletion,
            match_away_yellowCards=match.match_away_yellowCards, 
            match_away_redCards=match.match_away_redCards)
            
        match_new_list.append(match_new)
        #time.sleep(7)
    n += 1

db.session.add_all(match_new_list)
db.session.commit()

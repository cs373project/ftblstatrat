from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import time

import http
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError




#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

class Teams(db.Model):
    team_id = db.Column(db.Integer, primary_key=True)
    team_name = db.Column(db.String())
    team_country = db.Column(db.String())
    team_founded = db.Column(db.Integer)
    team_logoLink = db.Column(db.String())
    team_league = db.Column(db.String())
    team_record = db.Column(db.String())
    team_goals = db.Column(db.Integer)
    team_formation = db.Column(db.String())
    team_venueCity = db.Column(db.String())
    team_venueSurface = db.Column(db.String())
    team_venueCapacity = db.Column(db.Integer)
    team_venueName = db.Column(db.String())
    team_penaltyPercentage = db.Column(db.String())
    team_goalsForAverage = db.Column(db.String())
    team_goalsAgainstAverage = db.Column(db.String())
    team_form = db.Column(db.String())
    team_video = db.Column(db.String())



    def __init__(self, team_name = "NaN", team_country = "NaN", team_founded = 0, 
    team_logoLink = "NaN", team_league = "NaN", team_record = "NaN", team_goals = 0, team_formation = "NaN", team_venueCity = "NaN", team_venueSurface = "NaN", team_venueCapacity = 0,
    team_venueName="NaN", team_penaltyPercentage = "NaN", team_goalsForAverage = "NaN", team_goalsAgainstAverage = "NaN", team_form = "NaN", team_video = "NaN"):
        self.team_name = team_name
        self.team_country = team_country
        self.team_founded = team_founded
        self.team_logoLink = team_logoLink
        self.team_league = team_league
        self.team_record = team_record
        self.team_goals = team_goals
        self.team_formation = team_formation
        self.team_venueCity = team_venueCity
        self.team_venueSurface = team_venueSurface
        self.team_venueCapacity = team_venueCapacity
        self.team_venueName = team_venueName
        self.team_penaltyPercentage = team_penaltyPercentage
        self.team_goalsForAverage = team_goalsForAverage
        self.team_goalsAgainstAverage = team_goalsAgainstAverage
        self.team_form = team_form
        self.team_video = team_video


db.create_all()

cred = credentials.Certificate('./creds.json')
firebase_admin.initialize_app(cred)

fire_db = firestore.client()

data_ref = fire_db.collection(u'teams')
team_data = data_ref.stream()
teamData = []
for item in team_data:
    teamData.append(item.to_dict())

team_stats_ref = fire_db.collection(u'teamstats')
team_stats = team_stats_ref.stream()
teamStats = []
for item in team_stats:
    teamStats.append(item.to_dict())

DEVELOPER_KEY = os.getenv("YOUTUBE_API_KEY")
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./ytcreds.json"

def youtube_search(query, maxResults):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        q=query,
        part='id,snippet',
        maxResults=maxResults
    ).execute()

    for search_result in search_response.get('items', []):
        if search_result['id']['kind'] == 'youtube#video':
            return (f'https://www.youtube.com/watch?v={search_result["id"]["videoId"]}')

    return ''


team_list = []
cnt  = 0
for team in teamData:
    if (cnt < 91):
        cnt += 1
        continue

    if (cnt == 195):
        break
    teamid = team['team']['id']
    name = team['team']['name']
    logolink = team['team']['logo']
  
    country = team['team']['country']
    venueSurface = team['venue']['surface']
    venueCity = team['venue']['city']
    founded = team['team']['founded']
    venueCapacity = team['venue']['capacity']
    venueName = team['venue']['name']
    video = "N/A"
    try:
        video = youtube_search(f'{team["team"]["name"]} highlights', 5)
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
    


    # print(teamid, founded, name,logolink, country, venueCity, venueSurface)
    for stat in teamStats:
        idOfTeam = stat['team']['id']
        if (idOfTeam == teamid):
        
            league = stat['league']['name']
            if (len(stat['lineups']) >= 1):
                formation_temp = stat['lineups'][0]
                formation = formation_temp['formation']
            else:
                formation = "Unknown"

                    
            goals_temp = stat['goals']['for']
            goals = goals_temp['total']['total']

            loses = stat['fixtures']['loses']['total']
            wins = stat['fixtures']['wins']['total']
            draws = stat['fixtures']['draws']['total']
            record = str(wins) + '-' + str(loses) + '-' + str(draws)

            form = stat['form']
            goalsForAverage = stat['goals']['for']['average']['total']
            goalsAgainstAverage = stat['goals']['against']['average']['total']
            print(video)
            penaltyPercentage = stat['penalty']['scored']['percentage']
            new_team = Teams(team_name = name, team_country = country, team_founded = founded, team_logoLink = logolink, team_league = league, team_record = record,
            team_goals = goals, team_formation = formation, team_venueCity = venueCity, team_venueSurface = venueSurface, team_venueCapacity =  venueCapacity, team_venueName = venueName,
            team_penaltyPercentage = penaltyPercentage, team_goalsForAverage = goalsForAverage, team_goalsAgainstAverage = goalsAgainstAverage, team_form = form, team_video = video)
            team_list.append(new_team)
            break
        else:
            pass
    cnt+=1

db.session.add_all(team_list)
db.session.commit()





from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import time
import http
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

class Match(db.Model):
    match_id = db.Column(db.Integer, primary_key=True)
    match_score = db.Column(db.String())
    match_prediction = db.Column(db.String())
    match_location = db.Column(db.String())
    match_video = db.Column(db.String())
    match_home_name = db.Column(db.String())
    match_home_players = db.Column(db.String())
    match_home_shotsOnGoal = db.Column(db.Integer)
    match_home_shotsOffGoal = db.Column(db.Integer)
    match_home_cornerKicks = db.Column(db.Integer)
    match_home_posessionPercentage = db.Column(db.String())
    match_home_passCompletion = db.Column(db.String())
    match_home_yellowCards = db.Column(db.Integer)
    match_home_redCards = db.Column(db.Integer)
    match_away_name = db.Column(db.String())
    match_away_players = db.Column(db.String())
    match_away_shotsOnGoal = db.Column(db.Integer)
    match_away_shotsOffGoal = db.Column(db.Integer)
    match_away_cornerKicks = db.Column(db.Integer)
    match_away_posessionPercentage = db.Column(db.String())
    match_away_passCompletion = db.Column(db.String())
    match_away_yellowCards = db.Column(db.Integer)
    match_away_redCards = db.Column(db.Integer)
    
    def __init__(self, match_score = "NaN", match_prediction = "NaN", match_location = "NaN",
        match_video = "NaN", match_home_name = "NaN", match_home_players = "NaN", 
        match_home_shotsOnGoal = 0, match_home_shotsOffGoal = 0, match_home_cornerKicks = 0, 
        match_home_posessionPercentage = "NaN", match_home_passCompletion = "NaN", match_home_yellowCards = 0, 
        match_home_redCards = 0, match_away_name = "NaN", match_away_players = "NaN", 
        match_away_shotsOnGoal = 0, match_away_shotsOffGoal = 0, match_away_cornerKicks = 0, 
        match_away_posessionPercentage = "NaN", match_away_passCompletion = "NaN", match_away_yellowCards = 0, 
        match_away_redCards = 0):
            self.match_score = match_score
            self.match_prediction = match_prediction
            self.match_location = match_location
            self.match_video = match_video
            self.match_home_name = match_home_name
            self.match_home_players = match_home_players
            self.match_home_shotsOnGoal = match_home_shotsOnGoal
            self.match_home_shotsOffGoal = match_home_shotsOffGoal
            self.match_home_cornerKicks = match_home_cornerKicks
            self.match_home_posessionPercentage = match_home_posessionPercentage
            self.match_home_passCompletion = match_home_passCompletion
            self.match_home_yellowCards = match_home_yellowCards
            self.match_home_redCards = match_home_redCards
            self.match_away_name = match_away_name
            self.match_away_players = match_away_players
            self.match_away_shotsOnGoal = match_away_shotsOnGoal
            self.match_away_shotsOffGoal = match_away_shotsOffGoal
            self.match_away_cornerKicks = match_away_cornerKicks
            self.match_away_posessionPercentage = match_away_posessionPercentage
            self.match_away_passCompletion = match_away_passCompletion
            self.match_away_yellowCards = match_away_yellowCards
            self.match_away_redCards = match_away_redCards

db.create_all()

cred = credentials.Certificate('./creds.json')
firebase_admin.initialize_app(cred)

fire_db = firestore.client()

data_ref = fire_db.collection(u'matches')
match_data = data_ref.stream()

match_stats_ref = fire_db.collection(u'matchstats')
match_stats = match_stats_ref.stream()

conn = http.client.HTTPSConnection("v3.football.api-sports.io")

# for stat in match_stats:
#      print(' ')
#      print(stat.to_dict())


# youtube stuff
DEVELOPER_KEY = os.getenv("YOUTUBE_API_KEY")
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./ytcreds.json"

def youtube_search(query, maxResults):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        q=query,
        part='id,snippet',
        maxResults=maxResults
    ).execute()

    for search_result in search_response.get('items', []):
        if search_result['id']['kind'] == 'youtube#video':
            return (f'https://www.youtube.com/watch?v={search_result["id"]["videoId"]}')

    return ''


match_list = []

n = 0
for match in match_data:
    if n > 205:
        break
    elif n >= 200:
        match = match.to_dict()
        id=match["fixture"]["id"]
        score= f'{match["score"]["fulltime"]["home"]} - {match["score"]["fulltime"]["away"]}'
        location=match["fixture"]["venue"]["city"]
        prediction=match["fixture"]["prediction"]
        home_name=match["teams"]["home"]["name"]
        away_name=match["teams"]["away"]["name"]
        #home_id=match["teams"]["home"]["id"]
        #away_id=match["teams"]["away"]["id"]
        #video=match["fixture"]["video"]

        try:
            video = youtube_search(f'{match["teams"]["home"]} vs {match["teams"]["away"]} highlights', 5)
            #print(video)
        except HttpError as e:
            print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
    
        headers = {
                'x-rapidapi-host': "v3.football.api-sports.io",
                'x-rapidapi-key': "a3ac3b1e56a39e5e4ba30ab84bc8597b"
                }

        conn.request("GET", f'/fixtures/statistics?fixture={match["fixture"]["id"]}', headers=headers)

        res = conn.getresponse()
        data = res.read()

        dataVal = json.loads(data.decode("utf-8"))
        print(dataVal)
        print()
        dataVal = dataVal["response"]

        home_shotsOnGoal=dataVal[0]["statistics"][0]["value"]
        home_shotsOffGoal=dataVal[0]["statistics"][1]["value"]
        home_cornerKicks=dataVal[0]["statistics"][7]["value"]
        home_posessionPercentage=dataVal[0]["statistics"][9]["value"]
        home_passCompletion=dataVal[0]["statistics"][15]["value"]
        home_yellowCards=dataVal[0]["statistics"][10]["value"]
        home_redCards=dataVal[0]["statistics"][11]["value"]
        away_shotsOnGoal=dataVal[1]["statistics"][0]["value"]
        away_shotsOffGoal=dataVal[1]["statistics"][1]["value"]
        away_cornerKicks=dataVal[1]["statistics"][7]["value"]
        away_posessionPercentage=dataVal[1]["statistics"][9]["value"]
        away_passCompletion=dataVal[1]["statistics"][15]["value"]
        away_yellowCards=dataVal[1]["statistics"][10]["value"]
        away_redCards=dataVal[1]["statistics"][11]["value"]

        conn.request("GET", f'/fixtures/lineups?fixture={match["fixture"]["id"]}', headers=headers)

        res = conn.getresponse()
        data = res.read()

        dataVal = json.loads(data.decode("utf-8"))
        dataVal = dataVal["response"]
        
        home_players=""
        for player in dataVal[0]["startXI"]:
            home_players+=str(player["player"]["id"])+" "
        
        away_players=""
        for player in dataVal[1]["startXI"]:
            away_players+=str(player["player"]["id"])+" "
        
        new_match = Match(
            match_score=score, 
            match_location=location, 
            match_video=video,
            match_home_name=home_name, 
            match_home_players=home_players, 
            match_home_shotsOnGoal=home_shotsOnGoal, 
            match_home_shotsOffGoal=home_shotsOffGoal, 
            match_home_cornerKicks=home_cornerKicks, 
            match_home_posessionPercentage=home_posessionPercentage, 
            match_home_passCompletion=home_passCompletion, 
            match_home_yellowCards=home_yellowCards, 
            match_home_redCards=home_redCards,
            match_away_name=away_name,
            match_away_players=away_players,
            match_away_shotsOnGoal=away_shotsOnGoal,
            match_away_shotsOffGoal=away_shotsOffGoal,
            match_away_cornerKicks=away_cornerKicks, 
            match_away_posessionPercentage=away_posessionPercentage, 
            match_away_passCompletion=away_passCompletion,
            match_away_yellowCards=away_yellowCards, 
            match_away_redCards=away_redCards)
            
        match_list.append(new_match)
        time.sleep(12)
    n += 1

db.session.add_all(match_list)
db.session.commit()

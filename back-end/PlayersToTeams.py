from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields
import flask
import json
import flask_marshmallow as ma
from dotenv import load_dotenv
import os

import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json

#Initialize app
app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#db
load_dotenv()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY")

db = SQLAlchemy(app)

class PlayerTeam(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    team_id = db.Column(db.Integer)

    def __init__(self, player_id = 0, team_id = 0):
        self.player_id = player_id
        self.team_id = team_id

class Player(db.Model):
    player_id = db.Column(db.Integer, primary_key=True)
    player_name = db.Column(db.String())
    player_age = db.Column(db.Integer)
    player_height = db.Column(db.String())
    player_weight = db.Column(db.String())
    player_position = db.Column(db.String())
    player_teamName = db.Column(db.String())
    player_teamID = db.Column(db.Integer)
    player_jerseyNumber = db.Column(db.String())
    player_hometown = db.Column(db.String())
    player_country = db.Column(db.String())
    player_games = db.Column(db.Integer)
    player_goals = db.Column(db.Integer)
    player_photo = db.Column(db.String())

class Teams(db.Model):
    team_id = db.Column(db.Integer, primary_key=True)
    team_name = db.Column(db.String())
    team_country = db.Column(db.String())
    team_founded = db.Column(db.Integer)
    team_logoLink = db.Column(db.String())
    team_league = db.Column(db.String())
    team_record = db.Column(db.String())
    team_goals = db.Column(db.Integer)
    team_formation = db.Column(db.String())
    team_venueCity = db.Column(db.String())
    team_venueSurface = db.Column(db.String())
    
db.create_all()

player_team_list = []

for player, team in db.session.query(Player.player_id, Player.player_teamName).all():
    id = 0

    item = db.session.query(Teams).filter_by(team_name=team).first()
    if item is not None:
        id = item.team_id

    new_player_team = PlayerTeam(
        player_id = player,
        team_id = id,
    )
    player_team_list.append(new_player_team)

db.session.add_all(player_team_list)
db.session.commit()

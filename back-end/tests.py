#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
import sys, os
import requests


class Tests(TestCase):
    def test_allPlayers(self):
        r = requests.get("https://api.football-stats.me/players?perPage=4000")
        assert r.status_code == 200
        playerData = r.json()
        assert len(playerData["players"]) == 3740
        assert playerData["players"][1] == {
            "player_age": 28,
            "player_country": "Mexico",
            "player_games": 8,
            "player_goals": 1,
            "player_height": "188 cm",
            "player_hometown": "Guadalajara",
            "player_id": 2,
            "player_jerseyNumber": None,
            "player_name": "C. Salcedo",
            "player_photo": "https://media.api-sports.io/football/players/2877.png",
            "player_position": "Defender",
            "player_teamName": "Tigres UANL",
            "player_weight": "77 kg",
        }

    def test_playerById(self):
        r = requests.get("https://api.football-stats.me/players/id=24")
        assert r.status_code == 200
        playerData = r.json()
        assert playerData == {
            "player_age": 20,
            "player_country": "England",
            "player_games": 0,
            "player_goals": 0,
            "player_height": "186 cm",
            "player_hometown": "Chester",
            "player_id": 24,
            "player_jerseyNumber": None,
            "player_name": "B. Thomas",
            "player_photo": "https://media.api-sports.io/football/players/264437.png",
            "player_position": "Defender",
            "player_teamName": "Burnley",
            "player_weight": None,
        }

    def test_allMatches(self):
        r = requests.get("https://api.football-stats.me/matches?perPage=300")
        assert r.status_code == 200
        matchData = r.json()
        assert len(matchData["matches"]) == 203
        assert matchData["matches"][0] == {
            "match_away_cornerKicks": 4, 
            "match_away_name": "GO Ahead Eagles", 
            "match_away_passCompletion": "76%", 
            "match_away_players": "36968 43076 36887 129067 47279 37833 37460 37651 600 47393 37839 ", 
            "match_away_posessionPercentage": "45%", 
            "match_away_redCards": None, 
            "match_away_shotsOffGoal": 5, 
            "match_away_shotsOnGoal": 3, 
            "match_away_yellowCards": 4, 
            "match_date": "2021-10-02T16:45:00+00:00", 
            "match_home_cornerKicks": 5, 
            "match_home_name": "Waalwijk", 
            "match_home_passCompletion": "76%", 
            "match_home_players": "36854 37114 36857 8777 36865 36788 37619 36862 158053 38729 30542 ", 
            "match_home_posessionPercentage": "55%", 
            "match_home_redCards": None, 
            "match_home_shotsOffGoal": 6, 
            "match_home_shotsOnGoal": 3, 
            "match_home_yellowCards": 2, 
            "match_id": 3, 
            "match_league": "Eredivisie", 
            "match_location": "Waalwijk", 
            "match_prediction": "1", 
            "match_score": "1 - 1", 
            "match_venue_name": "Mandemakers Stadion", 
            "match_video": "https://www.youtube.com/watch?v=IZ3lWWl0rIA",
        }

    def test_matchById(self):
        r = requests.get("https://api.football-stats.me/matches/id=100")
        assert r.status_code == 200
        matchData = r.json()
        assert matchData["match_away_name"] == "Everton"

    def test_allTeams(self):
        r = requests.get("https://api.football-stats.me/teams?perPage=200")
        assert r.status_code == 200
        teamData = r.json()
        assert len(teamData["teams"]) == 179

        assert teamData["teams"][0] == {
            "team_country": "USA",
            "team_form": "DDWDWWLWWWLLDWLWDDDWDWLLLLDW",
            "team_formation": "4-2-3-1",
            "team_founded": 2008,
            "team_goals": 41,
            "team_goalsAgainstAverage": "1.5",
            "team_goalsForAverage": "1.5",
            "team_id": 1,
            "team_league": "Major League Soccer",
            "team_logoLink": "https://media.api-sports.io/football/teams/1598.png",
            "team_name": "Orlando City SC",
            "team_penaltyPercentage": "100.00%",
            "team_record": "11-8-9",
            "team_venueCapacity": 25527,
            "team_venueCity": "Orlando, Florida",
            "team_venueName": "Exploria Stadium",
            "team_venueSurface": "grass",
            "team_video": "https://www.youtube.com/watch?v=7zSz1bPhkNA",
        }

    def test_teamById(self):
        r = requests.get("https://api.football-stats.me/teams/id=45")
        assert r.status_code == 200
        teamData = r.json()

        assert teamData == {
            "team_country": "USA",
            "team_form": "LLWWLLWWLDWDDLLDLLWLDLWDWDWW",
            "team_formation": "4-3-1-2",
            "team_founded": 1995,
            "team_goals": 34,
            "team_goalsAgainstAverage": "1.1",
            "team_goalsForAverage": "1.2",
            "team_id": 45,
            "team_league": "Major League Soccer",
            "team_logoLink": "https://media.api-sports.io/football/teams/1602.png",
            "team_name": "New York Red Bulls",
            "team_penaltyPercentage": "100.00%",
            "team_record": "10-11-7",
            "team_venueCapacity": 26765,
            "team_venueCity": "Harrison, New Jersey",
            "team_venueName": "Red Bull Arena",
            "team_venueSurface": "grass",
            "team_video": "https://www.youtube.com/watch?v=gEg9VdzaNxw",
        }


if __name__ == "__main__":  # pragma: no cover
    main()

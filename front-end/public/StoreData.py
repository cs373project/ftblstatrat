import http.client
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
import time
from datetime import datetime, timedelta, timezone
import pytz
import requests

# api_tz = pytz.timezone("Europe/London")
# # Change this to your timezone
# local_tz = pytz.timezone("America/Chicago")

# # Use a service account
# cred = credentials.Certificate('./creds.json')
# firebase_admin.initialize_app(cred)

# db = firestore.client()

# conn = http.client.HTTPSConnection("v3.football.api-sports.io")

# data_ref = db.collection(u'matches')
# docs = data_ref.stream()

# i = 0

# startday = datetime.now(tz=timezone.utc).astimezone(api_tz) - timedelta(days=7)


# for param in docs:
#     if i >= 196:
#         time.sleep(10)
#         val = param.to_dict()
#         print(f'{val} : val\n')
#         headers = {
#             'x-rapidapi-host': "v3.football.api-sports.io",
#             'x-rapidapi-key': "x"
#             }

#         conn.request("GET", f'/fixtures?season=2021&last=2&team={val["team"]["id"]}&league={val["team"]["league_id"]}', headers=headers)

#         res = conn.getresponse()
#         data = res.read()

#         print(f'{data.decode("utf-8")}\n')
#         dataVal = json.loads(data.decode("utf-8"))
#         for n in range(2):
#             doc_ref = db.collection(u'matches').document(u'{}'.format(i+n))
#             doc_ref.set(dataVal["response"][n])

#     i += 2

# for param in docs:
#     if i >= 184 and i <= 195:
#         time.sleep(10)
#         val = param.to_dict()
#         print(f'{val} : val\n')
#         headers = {
#             'x-rapidapi-host': "v3.football.api-sports.io",
#             'x-rapidapi-key': "x"
#             }

#         conn.request("GET", f'/teams/statistics?season=2021&team={val["team"]["id"]}&league={val["team"]["league_id"]}', headers=headers)

#         res = conn.getresponse()
#         data = res.read()

#         print(f'{data.decode("utf-8")}\n')
#         dataVal = json.loads(data.decode("utf-8"))
#         doc_ref = db.collection(u'teamstats').document(u'{}'.format(i))
#         doc_ref.set(dataVal["response"])

#     i += 1

# for param in docs:
#     if i >= 0 and i <= 92:
#         val = param.to_dict()
#         print(f'{val} : val\n')
#         headers = {
#             'x-rapidapi-host': "v3.football.api-sports.io",
#             'x-rapidapi-key': "x"
#             }

#         conn.request("GET", f'/fixtures/statistics?fixture={val["fixture"]["id"]}', headers=headers)

#         res = conn.getresponse()
#         data = res.read()

#         print(f'{data.decode("utf-8")}\n')
#         dataVal = json.loads(data.decode("utf-8"))
#         doc_ref = db.collection(u'matchstats').document(u'{}'.format(i))
#         newDoc = {u'fixture': {u'id': param.fixture.id}, u'home': dataVal["response"][0], u'away': dataVal["response"][1]}
#         doc_ref.set(newDoc)
#         time.sleep(10)

#     i += 1

# for param in docs:
#     if i >= 3180 and i <= 3900:
#         val = param.to_dict()
#         print(f'{val} : val\n')
#         headers = {
#             'x-rapidapi-host': "v3.football.api-sports.io",
#             'x-rapidapi-key': "x"
#             }

#         conn.request("GET", f'/players?team={val["team"]["id"]}&season=2021&page=1', headers=headers)

#         res = conn.getresponse()
#         data = res.read()

#         print(f'{data.decode("utf-8")}\n')
#         dataVal = json.loads(data.decode("utf-8"))
#         for n in range(len(dataVal["response"])):
#             doc_ref = db.collection(u'players').document(u'{}'.format(i+n))
#             doc_ref.set(dataVal["response"][n])
#         time.sleep(10)

#     i += 20

# conn = http.client.HTTPSConnection("free-football-soccer-videos.p.rapidapi.com")

# for param in docs:
#     val = param.to_dict()
#     headers = {
#         'x-rapidapi-host': "free-football-soccer-videos.p.rapidapi.com",
#         'x-rapidapi-key': "x"
#         }

#     conn.request("GET", "/", headers=headers)

#     res = conn.getresponse()
#     data = res.read()

#     dataVal = json.loads(data.decode("utf-8"))
#     for n in range(len(dataVal)):
#         if (dataVal[n]["side1"]["name"] == val["teams"]["home"]["name"] and dataVal[n]["side2"]["name"] == val["teams"]["away"]["name"]) or (dataVal[n]["side2"]["name"] == val["teams"]["home"]["name"] and dataVal[n]["side1"]["name"] == val["teams"]["away"]["name"]):
#             doc_ref = db.collection(u'matches').document(u'{}'.format(i+n))
#             doc_ref.update({u'fixture': {u'video': dataVal[n]["url"]}})

# conn = http.client.HTTPSConnection("football-prediction-api.p.rapidapi.com")

# headers = {
#     'x-rapidapi-host': "football-prediction-api.p.rapidapi.com",
#     'x-rapidapi-key': "x"
#     }

# for n in range(14):
#     conn.request("GET", f'/api/v2/predictions?market=classic&iso_date={str(startday - timedelta(days=n + 7))[0:10]}&federation=UEFA', headers=headers)
#     print(str(startday))
#     res = conn.getresponse()
#     data = res.read()
    
#     for param in docs:
#         val = param.to_dict()
#         # print(data.decode("utf-8"))
#         for match in json.loads(data.decode("utf-8"))["data"]:
#             if (match["home_team"].lower() == val["teams"]["home"]["name"].lower() and match["away_team"].lower() == val["teams"]["away"]["name"].lower()) or (match["home_team"].lower() == val["teams"]["away"]["name"].lower() and match["away_team"].lower() == val["teams"]["home"]["name"].lower()):
#                 doc_ref = db.collection(u'matches').document(u'{}'.format(param.id))
#                 doc_ref.update({u'fixture.prediction': match["prediction"]})
#     time.sleep(6)
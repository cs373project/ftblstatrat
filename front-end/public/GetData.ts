// const axios = require("axios");
// const moment = require("moment-timezone");

// const RAPID_API_KEY = "x";
// const OTHER_API_KEY = "x"

// axios({
//     method: 'get',
//     url: 'https://v3.football.api-sports.io/teams/statistics',
//     headers: {
//         'x-rapidapi-key': OTHER_API_KEY,
//         'x-rapidapi-host': 'v3.football.api-sports.io'
//     },
//     params: {team: 1, league:39, season: 2021}
//     })
//     .then(function (response) {
//       console.log(JSON.stringify(response.data));
//     })
//     .catch(function (error) {
//     console.log(error);
// })

// const API_TZ = "America/Chicago";

// const now = moment.tz(API_TZ);
// const tomorrow = now.subtract(1, "days");
// // check past week? just subtract days
// // can be added programmatically by checking date from fixtures endpoint in football.api-sports.io

// axios({
//   method: 'get',
//   url: 'https://football-prediction-api.p.rapidapi.com/api/v2/predictions',
//   headers: {
//     'x-rapidapi-key': RAPID_API_KEY
//   },
//   params: {
//     market: "classic",
//     federation: "UEFA",
//     iso_date: "2021-08-28"// tomorrow.format("YYYY-MM-DD")
//   }
//   })
// .then(function (response) {
//   console.log(JSON.stringify(response.data));
// })
// .catch(function (error) {
//   console.log(error);
// });

// var options = {
//   method: 'get',
//   url: 'https://free-football-soccer-videos.p.rapidapi.com/',
//   headers: {
//     'x-rapidapi-host': 'free-football-soccer-videos.p.rapidapi.com',
//     'x-rapidapi-key': RAPID_API_KEY
//   }
// };

// axios.request(options).then(function (response) {
// 	console.log(response.data);
// }).catch(function (error) {
// 	console.error(error);
// });
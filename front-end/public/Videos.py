import os 
from dotenv import load_dotenv
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

load_dotenv()
DEVELOPER_KEY = os.getenv("YOUTUBE_API_KEY")
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "./creds.json"

def youtube_search(query, maxResults):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
        developerKey=DEVELOPER_KEY)

    # Call the search.list method to retrieve results matching the specified
    # query term.
    search_response = youtube.search().list(
        q=query,
        part='id,snippet',
        maxResults=maxResults
    ).execute()

    for search_result in search_response.get('items', []):
        if search_result['id']['kind'] == 'youtube#video':
            return (f'https://www.youtube.com/watch?v={search_result["id"]["videoId"]}')

    return ''


# TODO replace with database query (players, teams, matches)
data = [{"name": "Cristiano Ronaldo"}]

for item in data:
    try:
        video = youtube_search(f'{item["name"]} highlights', 5)
        # print(video)
        # TODO add to column of db model for player/team
    except HttpError as e:
        print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))

# for item in data:
#     try:
#         # not sure how this needs to be queried, need both home and away team name values for match highlights
#         video = youtube_search(f'{item["home_team"]} vs {item["away_team"]} highlights', 5)
#         # print(video)
#         # TODO add to column of db model for match
#     except HttpError as e:
#         print('An HTTP error %d occurred:\n%s' % (e.resp.status, e.content))
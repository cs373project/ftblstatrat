import { dividerClasses } from '@mui/material';
import {Component} from 'react';
import "./basics.css";
import PlayerInstance from './PlayerInstance';
import {TeamsChart, MatchChart, PlayersChart} from './visualizations/visualizations'

class Visualizations extends Component {

    render() {
        return (
            <div>
                <div className="Heading">
                    <h1 className="TitleOne">Visualizations</h1>
                </div>
                <TeamsChart></TeamsChart>
                <MatchChart></MatchChart>
                <PlayersChart></PlayersChart>
            </div>
        );
    }
}

export default Visualizations;
import {Component} from 'react';
import "./basics.css";

class Highlights extends Component {

    componentDidMount() {
    }

    render() {

       
        return (
            <div>
            <div className="Heading">
                <h1 className="TitleOne">Notable Matches and Highlights</h1>
            </div>
            <div>
                <p className="Text">
                    Football has a long and storied history, with so many memorable matches. Here are some of the most notable ones to get you started.
                </p>
                <h2 className="TitleTwo" style={{textAlign:"center"}}>Brazil 1 - 7 Germany (2014 FIFA World Cup)</h2>
                    <div className = "CenterContent"><iframe width="560" height="315" src="https://www.youtube.com/embed/TbDzouUuD8E?start=0"
                    title="Germany 7-1 Brazil 2014 world cup semifinal all goals and highlights" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/></div>
                    <br/>
                    <div className="Text">
                        <p>
                            Both Brazil and Germany reached the semi-finals with an unbeaten record in the tournament. 
                            The Brazilians' game in quarter-final with Colombia caused them to lose forward Neymar due to injury, 
                            and defender and captain Thiago Silva due to his second yellow cards in the tournament. Despite 
                            the nonappearance of these key players, a close match was predictable, because both teams were 
                            traditional FIFA World Cup powers, sharing eight tournaments won and having previously met in 
                            the 2002 FIFA World Cup Final, in which Brazil won 2–0 and earned their fifth FIFA World Cup title.
                        </p>
                        <p>
                            This match, however, ended in a surprisingly shocking loss for Brazil. Germany national team 
                            led 5–0 at half time, with four goals scored within six minutes, and consequently brought the 
                            score up to 7–0 in the second half. Brazil scored their only goal in the last minute, ending 
                            the match 7–1. Germany's play maker Toni Kroos was selected as the man of the match.
                        </p>
                    </div>
                    <h2 className="TitleTwo" style={{textAlign:"center"}}>Liverpool 5 - 4 Alaves (2001 UEFA Cup Final)</h2>
                    <div className = "CenterContent"><iframe width="560" height="315" src="https://www.youtube.com/embed/pKgJT8nqVSQ?start=0"
                    title="Germany 7-1 Brazil 2014 world cup semifinal all goals and highlights" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/></div>
                    <br/>
                    <div className="Text">
                        <p>
                        It was an exciting game, with the score tied 4-4 at the end of regulation. The match continued into extra time, and just when everyone was expecting a penalty shoot-out to decide the champion, Delfí Geli headed into his own goal. Thus, Liverpool won the match on the golden goal rule. The victory meant Liverpool won a treble consisting of the Football League Cup, FA Cup and UEFA Cup.
                        </p>
                    </div>
                    <h2 className="TitleTwo" style={{textAlign:"center"}}>Barcelona 5 - 0 Real Madrid (La Liga 2010-2011)</h2>
                    <div className = "CenterContent"><iframe width="560" height="315" src="https://www.youtube.com/embed/5mLnuCORMrU?start=0"
                    title="Germany 7-1 Brazil 2014 world cup semifinal all goals and highlights" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/></div>
                    <br/>
                    <div className="Text">
                        <p>
                        The match was held on 29 November 2010. Mourinho had just arrived at Real Madrid to contest glorious Guardiola’s Barcelona. This match is one of the greatest football matches of all time due to its result and presence of the best football players on the pitch.
                        </p>
                        <p>
                        For Mourinho, it was the worst defeat of his professional career in management. His unbeaten team in the season were dominated by a brilliant and masterful Barcelona performance.
                        </p>
                    </div>
            </div>
            </div>
        );
    }
}

export default Highlights;
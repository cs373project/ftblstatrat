import React, { useState, useEffect, useCallback } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter as Router, Switch, Route, withRouter, Link } from "react-router-dom";
import About from "./About";
import Basics from "./Basics";
import Teams from "./Teams";
import Players from "./Players";
import Matches from "./Matches";
import Home from "./Home";
import TeamInstance from "./TeamInstance";
import PlayerInstance from "./PlayerInstance";
import MatchInstance from "./MatchInstance";
// import Firebase from "./Firebase";
import Api from "./Api";
import { QueryParamProvider } from "use-query-params"
import { Alert } from "react-bootstrap";
import SearchPage from "./SearchPage";
import Highlights from './Highlights';
import Visualizations from './Visualizations';
import ProviderVisualizations from './ProviderVisualizations';

const dataNotFound = (
  <div className = "models-pad-for-nav">
    <Alert variant = "danger">Data for this page is not yet supported</Alert>
    <div className = "models-center-content">
      <Link to="/">Back home</Link>
    </div>
  </div>
  
);

function App() {
    const [playerId, setPlayerId] = useState(1);
    const [teamId, setTeamId] = useState(1);
    const [matchId, setMatchId] = useState(1);
    
  return ( 
    <Router>
      <QueryParamProvider ReactRouterRoute={Route}>
      <Navbar />
        <Switch>
            <Route exact path="/" component = { withRouter(Home) }/>
            <Route path="/about" component = { About }/>
            <Route path="/basics" component = { withRouter(Basics) }/>
            <Route path="/highlights" component = { Highlights }/>
            <Route path="/visualizations" component = { Visualizations }/>
            <Route path="/providerVisualizations" component = { ProviderVisualizations }/>
            <Route path="/teams" render = { () => (<Teams setId = {setTeamId} id = {teamId}/>) }/>
            <Route path="/players" render = { () => (<Players setId = {setPlayerId} id = {playerId}/>) }/>
            <Route path="/matches" render = { () => (<Matches setId = {setMatchId} id = {matchId}/>) }/>
            <Route path="/teaminstance" render = { () => (<TeamInstance id = {teamId} dataNotFound = {dataNotFound} setMatchId = {setMatchId} 
              setPlayerId = {setPlayerId}/>) }/>
            <Route path="/playerinstance" render = { () => (<PlayerInstance id = {playerId} dataNotFound = {dataNotFound} setTeamId = {setTeamId} 
              setMatchId = {setMatchId}/>) }/>
            <Route path="/matchinstance" render = { () => (<MatchInstance id = {matchId} setPlayerId = {setPlayerId} 
              setTeamId = {setTeamId} dataNotFound = {dataNotFound}/>) }/>

            <Route path="/search" render = { () => (<SearchPage setTeamId = {setTeamId} setMatchId = {setMatchId} setPlayerId = {setPlayerId}/>) }/>
        </Switch>
      </QueryParamProvider>
    </Router>
  );
}

export default App;

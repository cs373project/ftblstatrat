import axios from "axios";

const API_URL = "https://api.football-stats.me/" //"http://website-api-dev.eba-manpc2ja.us-west-2.elasticbeanstalk.com/" 

async function getModelData(endpoint: string, index: any, params: any, callback: any) {
    let data = {} as any;
    data = await axios({
        method: 'get',
        url: API_URL + "/" + endpoint,
        params: params
    })
    .then(function (response: any) {
        let data_val = response.data;
        return data_val;
    })
    .catch(function (error: any) {
        console.log(error);
        return {};
    });
    if (index) {
        return data[index];
    } 
    return data;
}

const Api = { getModelData };

export default Api;
import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import { Button, ListGroup, Tooltip, OverlayTrigger } from "react-bootstrap";
import { BsFillQuestionCircleFill } from "react-icons/bs";
import { useHistory } from "react-router-dom";
import Loader from "./Loader";
import Api from "./Api";
import { Paper, Grid } from "@mui/material";

function TeamInstance(props: any) {
  const history = useHistory();
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({} as any);
  const [players, setPlayers] = useState([] as any);
  const [videoUrl, setVideoUrl] = useState("");
  const [recentMatches, setRecentMatches] = useState([] as any);

  // call stubhub api and retrieve first upcoming event
  const [events, setEvents] = useState("");
  const [price, setPrice] = useState(0);

  let url = "https://api.stubhub.com/sellers/search/events/v3?name=" + data.team_name + "&sort=eventDateLocal%20asc"
  url = url.replaceAll(" ", "%20")

  fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer qKhVQL4jtXx9VpNNqDnJRhGLGAAm'
    },
  })
  .then(res => res.json())
  .then((data1) => {
    if (data1.numFound > 0) {
      setEvents(data1.events[0].webURI);
      console.log(data1.events[0].ticketInfo);
      setPrice(Math.trunc(data1.events[0].ticketInfo.minListPrice));
    } else {
      setEvents("n/a")
    }
  })
  .catch(console.log);

  useEffect(() => {
    setLoading(false);
  }, [events, price]);

  function handleMatchClick(e: any) {
    props.setMatchId(e.target.id);
  }

  function handlePlayerClick(e: any) {
    props.setPlayerId(e.target.id);
  }

  useEffect(() => {
    const getData = async () => {
      try {
				setLoading(true)
        const data = await Api.getModelData("teams/id=" + props.id, undefined, {}, undefined);
        const matchTeamData = await Api.getModelData("matches/teams", "matchteam ids", {}, undefined);
        const playerTeamData = await Api.getModelData("players/teams", "playerteam ids", {}, undefined);
        const playerData = await Api.getModelData("players", "players", {perPage: 4000}, undefined);
        const matchData = await Api.getModelData("matches", "matches", {perPage: 4000}, undefined);
				setData(data)
        if (data) {
          let matchIds = [] as any;
          matchTeamData.forEach((val: any) => val.team_home_id === data.team_id.toString() || val.team_away_id === data.team_id.toString() ? matchIds.push(val.match_id) : undefined);
          let recents = (matchData.filter((val: any) => matchIds.includes(val.match_id)))
          setRecentMatches(recents);
          const playerTeams = playerTeamData.filter((otherVal: any) => otherVal.team_id === data.team_id.toString());
          setPlayers(playerData.filter((val: any) => playerTeams.find((otherVal: any) => otherVal.player_id === val.player_id) !== undefined));
          setVideoUrl(data.team_video.substring(0, data.team_video.indexOf("watch")) + "embed/" + data.team_video.substring(data.team_video.indexOf("watch") + 8, data.team_video.length));
        }
        
				setLoading(false)
			} catch (err) {
				console.error(err);
			}
    }
    getData();
  }, [history, props.id]);

  return ( loading ? <Loader /> : !data ? props.dataNotFound :
    <div className = "models-instance">
      <div className = "models-center-content"><img src = {data.team_logoLink} alt = {data.team_name}/></div>
      <div className = "models-center-list-group">
        <ListGroup horizontal>
          <ListGroup.Item>Name</ListGroup.Item>
          <ListGroup.Item>{data.team_name}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Form</ListGroup.Item>
          <ListGroup.Item>{data.team_form}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Founded</ListGroup.Item>
          <ListGroup.Item>{data.team_founded}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Country</ListGroup.Item>
          <ListGroup.Item>{data.team_country}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Home</ListGroup.Item>
          <ListGroup.Item>{data.team_venueCity}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Venue</ListGroup.Item>
          <ListGroup.Item>{data.team_venueName}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Venue capacity</ListGroup.Item>
          <ListGroup.Item>{data.team_venueCapacity}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>League</ListGroup.Item>
          <ListGroup.Item>{data.team_league}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Record (Win-Lose-Draw)</ListGroup.Item>
          <ListGroup.Item>{data.team_record}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Total Goals</ListGroup.Item>
          <ListGroup.Item>{data.team_goals}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Average Goal Difference  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "goal-difference-tooltip">
                  The difference between the number of goals the team scored and the number of goals conceded, a positive value meaning more goals scored than conceded
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger></ListGroup.Item>
          <ListGroup.Item>{Math.round((data.team_goalsForAverage - data.team_goalsAgainstAverage) * 100) / 100}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Most Common Formation  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "formation-tooltip">
                  A representation of how the players are organized on the field for this team, where the first number is defenders, then midfielders, and forwards
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger></ListGroup.Item>
          <ListGroup.Item>{data.team_formation}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Penalty percentage</ListGroup.Item>
          <ListGroup.Item>{data.team_penaltyPercentage}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Team's recent games</ListGroup.Item>
          <ListGroup.Item>{
                  recentMatches.map((val: any) => 
                    <Link to="/matchinstance">
                      <Button variant = "link" id = { val.match_id } onClick = { handleMatchClick } >
                        {val.match_home_name} vs {val.match_away_name},  
                      </Button>
                    </Link>
                  )}
          </ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Known players</ListGroup.Item>
          <ListGroup.Item>{
                  (players.map((val: any) => 
                    <Link to="/playerinstance">
                      <Button variant = "link" id = { val.player_id } onClick = { handlePlayerClick } >
                        {val.player_name} - {val.player_position}, 
                      </Button>
                    </Link>
                  ))
                  }</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Buy tickets to upcoming matches!</ListGroup.Item>
          <ListGroup.Item><a href = {"https://www.stubhub.com/" + events}>{events === "n/a" ? "No tickets available on StubHub" : "Tickets starting from $" + price}</a></ListGroup.Item>
        </ListGroup>
      </div>
      <div className = "models-instance-bottom">
        <div className = "models-listgroup-inline">
          <div className = "models-center-radar-header"><h1 className = "models-title">Highlights</h1></div>
          <div className = "models-center-radar">
            <iframe width="560" height="315" src={videoUrl}
                    title={data.match_home_name + "vs" + data.match_away_name + " Highlights"} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/>
          </div>
        </div>
        <div className = "models-listgroup-inline">
          <div className = "models-center-radar-header"><h1 className = "models-title">Venue Map</h1></div>
          <div className = "models-center-radar">
          <iframe
            width="560"
            height="315"
            src={data.team_venueName ? "https://www.google.com/maps/embed/v1/place?key=AIzaSyCvl78QdYv3cgOclHhIm0FsF8qmTdnpAkA&q=" + data.team_venueName.replace(" ", "+") : undefined}  allowFullScreen>
          </iframe>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TeamInstance;
import React, { useState, useEffect } from 'react';
import "./App.css"
import { Link, useHistory} from "react-router-dom";
import { Button, ListGroup, OverlayTrigger, Tooltip } from "react-bootstrap";
import { BsFillQuestionCircleFill } from "react-icons/bs";
import Loader from "./Loader";
import Api from "./Api";

function MatchInstance(props: any) {
  const history = useHistory();
  const [videoUrl, setVideoUrl] = useState("");
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState({} as any);
  const [homeTeam, setHomeTeam] = useState({} as any);
  const [homePlayers, setHomePlayers] = useState({} as any);
  const [awayTeam, setAwayTeam] = useState({} as any);
  const [awayPlayers, setAwayPlayers] = useState({} as any);

  function handleTeamClick(e: any) {
    props.setTeamId(e.target.id);
  }

  function handlePlayerClick(e: any) {
    props.setPlayerId(e.target.id);
  }

  useEffect(() => {
    const getData = async () => {
      try {
				setLoading(true)
        const data = await Api.getModelData("matches/id=" + props.id, undefined, {}, undefined);
        const matchTeamData = await Api.getModelData("matches/teams", "matchteam ids", {}, undefined);
        const playerTeamData = await Api.getModelData("players/teams", "playerteam ids", {}, undefined);
        const teamData = await Api.getModelData("teams", "teams", {perPage: 4000}, undefined);
        const playerData = await Api.getModelData("players", "players", {perPage: 4000}, undefined);
				setData(data)
        if (data && teamData) {
          const homeTeamId = matchTeamData.find((val: any) => val.match_id === data.match_id).team_home_id;
          const awayTeamId = matchTeamData.find((val: any) => val.match_id === data.match_id).team_away_id;
          setHomeTeam(teamData.find((val: any) => val.team_id === Number(homeTeamId)));
          setAwayTeam(teamData.find((val: any) => val.team_id === Number(awayTeamId)));
          const homePlayerTeams = playerTeamData.filter((otherVal: any) => otherVal.team_id === homeTeamId);
          setHomePlayers(playerData.filter((val: any) => homePlayerTeams.find((otherVal: any) => otherVal.player_id === val.player_id) !== undefined));
          const awayPlayerTeams = playerTeamData.filter((otherVal: any) => otherVal.team_id === awayTeamId);
          setAwayPlayers(playerData.filter((val: any) => awayPlayerTeams.find((otherVal: any) => otherVal.player_id === val.player_id) !== undefined));
          setVideoUrl(data.match_video.substring(0, data.match_video.indexOf("watch")) + "embed/" + data.match_video.substring(data.match_video.indexOf("watch") + 8, data.match_video.length));
        }
         
				setLoading(false)
			} catch (err) {
				console.error(err);
			}
    }
    getData();
  }, [history, props.id]);

  return ( loading ? <Loader /> : !data ? props.dataNotFound :
    <div className = "models-instance">
      <div className = "models-center-content"><h1 className = "models-title">{data.match_home_name} vs {data.match_away_name}</h1></div>
      <div className = "models-center-list-group">
        <ListGroup horizontal>
          <ListGroup.Item>Score</ListGroup.Item>
          <ListGroup.Item>{data.match_home_name} {data.match_score} {data.match_away_name}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>1X2 Prediction  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "1X2-tooltip">
                  Form of prediction where you can choose one or two events to happen, where: 1 = Home win, X = Tie, 2 = Away win
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger>
          </ListGroup.Item>
          <ListGroup.Item>{data.match_prediction}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal className = "models-list-end">
          <ListGroup.Item>Location</ListGroup.Item>
          <ListGroup.Item>{data.match_location}</ListGroup.Item>
        </ListGroup>
        
        <ListGroup horizontal>
          <ListGroup.Item>Home team</ListGroup.Item>
          <ListGroup.Item><Link to={"/teaminstance/id=" + homeTeam.team_id}>
                      <Button variant = "link" id = { homeTeam && homeTeam.team_id } onClick = { handleTeamClick }>
                        {data.match_home_name}
                      </Button>
                    </Link><img className = "models-resize-team-logo" src = {homeTeam && homeTeam.team_logoLink} alt = {data.match_home_name}/></ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Known players</ListGroup.Item>
          <ListGroup.Item>{
                  (homePlayers.map((val: any) => 
                    <Link to="/playerinstance">
                      <Button variant = "link" id = { val.player_id } onClick = { handlePlayerClick } >
                        {val.player_name}
                      </Button>
                    </Link>
                  ))
                  }</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Shots on goal</ListGroup.Item>
          <ListGroup.Item>{data.match_home_shotsOnGoal}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Shots off goal</ListGroup.Item>
          <ListGroup.Item>{data.match_home_shotsOffGoal}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Corner Kicks</ListGroup.Item>
          <ListGroup.Item>{data.match_home_cornerKicks}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Possession %  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "possession-home-tooltip">
                  Calculated by dividing the home team's passes over the total number of passes in the game to get an estimate of how long each team had the ball
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger></ListGroup.Item>
          <ListGroup.Item>{data.match_home_posessionPercentage}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Pass completion %  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "pass-completion-home-tooltip">
                  Percentage of home team's passes that successfully reach a teammate
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger></ListGroup.Item>
          <ListGroup.Item>{data.match_home_passCompletion}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Yellow cards</ListGroup.Item>
          <ListGroup.Item>{data.match_home_yellowCards ? data.match_home_yellowCards : 0}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal className = "models-list-end">
          <ListGroup.Item>Red cards</ListGroup.Item>
          <ListGroup.Item>{data.match_home_redCards ? data.match_home_redCards : 0}</ListGroup.Item>
        </ListGroup>

        <ListGroup horizontal>
          <ListGroup.Item>Away team</ListGroup.Item>
          <ListGroup.Item><Link to={"/teaminstance/id=" + awayTeam.team_id}>
                      <Button variant = "link" id = { awayTeam && awayTeam.team_id } onClick = { handleTeamClick } >
                        {data.match_away_name}
                      </Button>
                    </Link><img className = "models-resize-team-logo" src = {awayTeam && awayTeam.team_logoLink} alt = {data.match_away_name}/></ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Known players</ListGroup.Item>
          <ListGroup.Item>{
                  (awayPlayers.map((val: any) => 
                    <Link to="/playerinstance">
                      <Button variant = "link" id = { val.player_id } onClick = { handlePlayerClick } >
                        {val.player_name}
                      </Button>
                    </Link>
                  ))
                  }</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Shots on goal</ListGroup.Item>
          <ListGroup.Item>{data.match_away_shotsOnGoal}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Shots off goal</ListGroup.Item>
          <ListGroup.Item>{data.match_away_shotsOffGoal}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Corner Kicks</ListGroup.Item>
          <ListGroup.Item>{data.match_away_cornerKicks}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Possession %  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "possession-away-tooltip">
                  Calculated by dividing the away team's passes over the total number of passes in the game to get an estimate of how long each team had the ball
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger></ListGroup.Item>
          <ListGroup.Item>{data.match_away_posessionPercentage}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Pass completion %  <OverlayTrigger
              placement="right"
              overlay={
                <Tooltip id = "pass-completion-away-tooltip">
                  Percentage of away team's passes that successfully reach a teammate
                </Tooltip>
              }
            >
              <BsFillQuestionCircleFill/>
            </OverlayTrigger></ListGroup.Item>
          <ListGroup.Item>{data.match_away_passCompletion}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Yellow cards</ListGroup.Item>
          <ListGroup.Item>{data.match_away_yellowCards ? data.match_away_yellowCards : 0}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Red cards</ListGroup.Item>
          <ListGroup.Item>{data.match_away_redCards ? data.match_away_redCards : 0}</ListGroup.Item>
        </ListGroup>
      </div>
      <div className = "models-center-radar-header"><h1 className = "models-title">Highlights</h1></div>
      <div className = "models-center-radar">
        <iframe width="560" height="315" src={videoUrl}
                title={data.match_home_name + "vs" + data.match_away_name + " Highlights"} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/>
      </div>
    </div>
  );
}

export default MatchInstance;
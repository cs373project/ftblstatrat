import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './home.css';
import background from "./images/homeBackground.jpg";
import stats from "./images/stats.png";
import chatBubbles from "./images/speechBubbles.jpg";
import player from "./images/soccerplayer.png";
import homePlayer from "./images/homePlayer.jpg";
import homeTeam from "./images/homeTeam.jpg";
import homeMatch from "./images/homeMatch.jpg";
import { Card } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { Button, Paper, Grid, TextField } from "@mui/material";
import {
	StringParam,
	useQueryParams,
} from "use-query-params";
import FadeInSection from './FadeInSection';

function Home() {
  const history = useHistory();
  const [searchParam, setSearchParam] = useState("");
  const [params, setParams] = useQueryParams({
    q: StringParam,
  })

  function handleCardClick(e: any) {
    console.log(e.target.id);
    history.push(e.target.id);
  }

  function handleSearch(e: any) {
    if (!e.key || e.key === "Enter") {
      setParams({...params, q: searchParam});
    }
  }

  function handleSearchInput(e: any) {
    setSearchParam(e.target.value);
  }

  useEffect(() => {
    const getData = async () => {
      try {
        if (params.q) {
          history.push("/search?q=" + params.q)
        }
			} catch (err) {
				console.error(err);
			}
    }
    getData();
  }, [history, params]);

  const cardData = [
    {
      img: homeTeam,
      title: "Teams",
      description: "Want to know more about a team's fundamentals? Scout their roster and season stats!",
      route: "/teams"
    },
    {
      img: homePlayer,
      title: "Players",
      description: "Learn all about your favorite players and their performance throughout the season!",
      route: "/players"
    },
    {
      img: homeMatch,
      title: "Matches",
      description: "Curious how your favorite team is playing? Check out the score and stats of recent matches!",
      route: "/matches"
    }
  ];
  return (
    // onMouseEnter, onMouseLeave set className to CardHover for body
    <div className = "App">
      <div className = "Wrapper">
        <div className = "Banner" style={{backgroundImage: `url(${background})`}}>
          <h1 className = "Title">FtblStatRat</h1>
          <h2 className = "Slogan">Indulge your inner football nerd</h2>
        </div>
      </div>
      <Grid container padding="3rem 0 4rem 0" justifyContent="center">
        <Paper className="paper" elevation={5} >
          <h1 className = "IntroText">
            Welcome to&nbsp;<span className = "IntroTextColor">FtblStatRat</span>!
          </h1>
          <p className = "SubheaderText">
            Search the entire site for any team, player, or match!
          </p>
          <div className = "models-center-space-evenly">
            <div className="search-bar">
              <TextField className = "search-field" type="text" placeholder="Search " value={searchParam} onChange={handleSearchInput} onKeyDown={handleSearch} variant="outlined"/>
            </div>
            <div className = "search-button-wrapper">
              <Button onClick={handleSearch} className = "search-button" variant="contained">Search</Button>
            </div>
          </div>
        </Paper>
      </Grid>
      <FadeInSection>
        <div className = "CardContainer">{cardData.map((val: any) => 
          <Card key = {val.title} className = "Card">
            <Card.Img variant = "top" src = {val.img} onClick = {handleCardClick} id = {val.route} className = "CardImage"/>
            <Card.Body onClick = {handleCardClick} id = {val.route} className = "">
              <Card.Title onClick = {handleCardClick} id = {val.route} className = "CardTitle">{val.title}</Card.Title>
              <Card.Text onClick = {handleCardClick} id = {val.route} className = "CardBody">{val.description}</Card.Text>
            </Card.Body>
          </Card>
        )}</div>
      </FadeInSection>
      <FadeInSection>
        <img className = "Icon" src={stats} alt = "Stats"/>
        <h3 className = "Caption">Look up stats</h3>
      </FadeInSection>
      <FadeInSection>
        <img className = "Icon" src={chatBubbles} alt = "Chat bubbles"/>
        <h3 className = "Caption">Connect with fellow fans</h3>
      </FadeInSection>
      <FadeInSection>
        <img className = "Icon" src={player} alt = "Player"/>
        <h3 className = "Caption">Relive your favorite football moments</h3>
      </FadeInSection>
    </div>
  );
}

export default Home;
import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, withRouter, Link } from "react-router-dom";
import renderer from 'react-test-renderer';
import { Alert } from "react-bootstrap";

/* importing components/pages to test */
import About from "../About";
import Players from "../Players";
import Matches from "../Matches";
import Basics from "../Basics";
import Home from "../Home";
import Teams from "../Teams";
import TeamInstance from "../TeamInstance";
import PlayerInstance from "../PlayerInstance";
import MatchInstance from "../MatchInstance";
import Loader from "../Loader";
import Highlights from "../Highlights";

/*Expect tests*/
// it('Get Home Page', async () => {
// 	const copy = renderer.create(<Home/>);
// 	expect(copy).toBeDefined();
// });

it('Get About Page', async () => {
	const copy = renderer.create(<About/>);
	expect(copy).toBeDefined();
});

it('Get Basics Page', async () => {
	const copy = renderer.create(<Basics />);
	expect(copy).toBeDefined();
});

// it('Get Teams Page', async () => {
// 	const copy = renderer.create(<Teams setId = {1}  data = {[]} id = {1} />);
// 	expect(copy).toBeDefined();
// });

// it('Get Players Page', async () => {
// 	const copy = renderer.create(<Players setId = {1} data = {[]} id = {1} />);
// 	expect(copy).toBeDefined();
// });

// it('Get Matches Page', async () => {
// 	const copy = renderer.create(<Matches setId = {1} data = {[]} id = {1}/>);
// 	expect(copy).toBeDefined();
// });

it('Get loader', async () => {
	const copy = renderer.create(<Loader/>);
	expect(copy).toBeDefined();
});

it('Get playerinstance', async () => {
	const copy = renderer.create(<PlayerInstance id = {1} dataNotFound = {dataNotFound} setTeamId = {1} 
		setMatchId = {1}/>);
	expect(copy).toBeDefined();
});

it('Get matchinstance', async () => {
	const copy = renderer.create(<MatchInstance id = {1} setPlayerId = {1} 
		setTeamId = {1} dataNotFound = {dataNotFound}/>);
	expect(copy).toBeDefined();
});

// it('Get teaminstance', async () => {
// 	const copy = renderer.create(<TeamInstance id = {1} dataNotFound = {dataNotFound} setMatchId = {1} 
// 		setPlayerId = {1}/>);
// 	expect(copy).toBeDefined();
// });

it('Get highlights', async () => {
	const copy = renderer.create(<Highlights />);
	expect(copy).toBeDefined();
});

/* Snapshot testing */
/* landing page test */
// it('Landing Page Snapshot', async () => {
// 	const tree = renderer
//     .create(<Home/>)
//     .toJSON();
//   	expect(tree).toMatchSnapshot();
// });

/* basics page test */
it('Basics Page Snapshot', async () => {
	const tree = renderer
    .create(<Basics/>)
    .toJSON();
  	expect(tree).toMatchSnapshot();
});

/* players page test */
// it('Players Page Snapshot', async () => {
// 	const tree = renderer
//     .create(<Players setId = {1} data = {[]} id = {1} />)
//     .toJSON();
//   	expect(tree).toMatchSnapshot();
// });

/* teams page test */
// it('Teams page Snapshot', async () => {
// 	const tree = renderer
//     .create(<Teams setId = {setTeamId} id = {teamId}/>)
//     .toJSON();
//   	expect(tree).toMatchSnapshot();
// });

/* matches page test */
// it('Matches page Snapshot', async () => {
// 	const tree = renderer
//     .create(<Matches setId = {1} />)
//     .toJSON();
//   	expect(tree).toMatchSnapshot();
// });

/*  loader test */
it('Loader Snapshot', async () => {
	const tree = renderer
    .create(<Loader/>)
    .toJSON();
  	expect(tree).toMatchSnapshot();
});

const dataNotFound = (
	<div className = "models-pad-for-nav">
	  <Alert variant = "danger">Data for this page is not yet supported</Alert>
	  <div className = "models-center-content">
		<Link to="/">Back home</Link>
	  </div>
	</div>
);

/* Match Instance page test */
it('Get Match Instance page', async () => {
	const tree = renderer
    .create(<MatchInstance id = {1} setPlayerId = {1} 
		setTeamId = {1} dataNotFound = {dataNotFound}/>)
    .toJSON();
  	expect(tree).toMatchSnapshot();
});
  
/* Players instance page test */
it('Get a Player Instance page', async () => {
	const tree = renderer
		.create(<PlayerInstance id = {1} dataNotFound = {dataNotFound} setTeamId = {1} 
			setMatchId = {1}/>)
    .toJSON();
  	expect(tree).toMatchSnapshot();
});

/* Teams instance page test */
// it('Get a Team Instance page', async () => {
// 	const tree = renderer
//     .create(<TeamInstance id = {1} dataNotFound = {dataNotFound} setMatchId = {1} 
// 		setPlayerId = {1}/>)
//     .toJSON();
//   	expect(tree).toMatchSnapshot();
// });

it('Get Highlights page', async () => {
	const tree = renderer
    .create(<Highlights />)
    .toJSON();
  	expect(tree).toMatchSnapshot();
});

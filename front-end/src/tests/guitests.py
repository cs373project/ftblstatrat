import selenium
import unittest
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import StaleElementReferenceException

# selenium tests for frontend code
class tests(unittest.TestCase):
    # homeUrl = "https://www.football-stats.me" # prod
    homeUrl = "https://development.d3mr5cf17ndbst.amplifyapp.com" # dev
    # chromedriver_path = "../../chromedriver-macm1" # use for local testing
    chromedriver_path = "../../chromedriver"


    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument("window-size=1920,1080")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(
            self.chromedriver_path, options=chrome_options)

    # def test_landing_buttons(self):
    #     # test buttons on landing page
    #     # players
    #     self.driver.get(self.homeUrl)
    #     self.driver.implicitly_wait(10)
    #     try:
    #         WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[3]/div[1]")))
    #         self.driver.execute_script("arguments[0].scrollIntoView();", WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[3]/div[1]"))))
    #         ActionChains(self.driver).move_to_element(WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[3]/div[1]")))).click().perform()
    #         assert self.homeUrl + "/players" in self.driver.current_url
    #     except StaleElementReferenceException:
    #         pass

    #     # teams
    #     self.driver.get(self.homeUrl)
    #     self.driver.implicitly_wait(10)
    #     self.driver.execute_script("window.scrollBy(0, 100);")
    #     self.driver.implicitly_wait(5)
    #     try:
    #         elem = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[3]/div[2]"
    #         )[0]
    #         ActionChains(self.driver).move_to_element(elem).click().perform()
    #         assert self.homeUrl + "/teams" in self.driver.current_url
    #     except StaleElementReferenceException:
    #         pass
    #     # matches
    #     self.driver.get(self.homeUrl)
    #     self.driver.implicitly_wait(10)
    #     self.driver.execute_script("window.scrollBy(0, 100);")
    #     self.driver.implicitly_wait(5)
    #     try:
    #         elem = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[3]/div[3]"
    #         )[0]
    #         ActionChains(self.driver).move_to_element(elem).click().perform()
    #         assert self.homeUrl + "/matches" in self.driver.current_url
    #     except StaleElementReferenceException:
    #         pass

    def test_landing_text(self):
        self.driver.get(self.homeUrl)
        self.driver.implicitly_wait(10)
        # title
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[1]/div/h1"
            )[0].text
            assert section == "FtblStatRat"
        except StaleElementReferenceException:
            pass

        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[1]/div/h2"
            )[0].text
            assert section == "Indulge your inner football nerd"
        except StaleElementReferenceException:
            pass

    def test_navBar(self):
        self.driver.get(self.homeUrl)
        self.driver.implicitly_wait(10)
        assert self.homeUrl in self.driver.current_url
        # test home page button
        self.driver.find_elements_by_xpath(
            "/html/body/div/nav/div/a[1]")[0].click()
        assert self.homeUrl in self.driver.current_url
        # test about button
        self.driver.find_elements_by_xpath(
            "/html/body/div/nav/div/a[2]")[0].click()
        assert self.homeUrl + "/about" in self.driver.current_url
        # test teams button
        self.driver.find_elements_by_xpath(
            "/html/body/div/nav/div/a[3]")[0].click()
        assert self.homeUrl + "/teams" in self.driver.current_url
        # test players button
        self.driver.find_elements_by_xpath(
            "/html/body/div/nav/div/a[4]")[0].click()
        assert self.homeUrl + "/players" in self.driver.current_url
         # test matches button
        self.driver.find_elements_by_xpath(
            "/html/body/div/nav/div/a[5]")[0].click()
        assert self.homeUrl + "/matches" in self.driver.current_url

    def test_about(self):
        # go to about page
        self.driver.get(self.homeUrl + "/about")
        self.driver.implicitly_wait(10)
        assert self.homeUrl + "/about" in self.driver.current_url
        # check different sections of the about page
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[1]/h1"
            )[0].text
            assert section == "About Us"
        except StaleElementReferenceException:
            pass
        
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/h2[1]"
            )[0].text
            assert section == "The Team"
        except StaleElementReferenceException:
            pass

        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/h2[2]"
            )[0].text
            assert section == "Project Stats"
        except StaleElementReferenceException:
            pass

        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/h2[3]"
            )[0].text
            assert section == "APIs and Datasets"
        except StaleElementReferenceException:
            pass

        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/h2[4]"
            )[0].text
            assert section == "Tools Used"
        except StaleElementReferenceException:
            pass
        
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/h2[5]"
            )[0].text
            assert section == "Links"
        except StaleElementReferenceException:
            pass

    def test_about_links(self):
        # go to about page
        self.driver.get(self.homeUrl + "/about")
        self.driver.implicitly_wait(10)
        assert self.homeUrl + "/about" in self.driver.current_url
        try:
            links = self.driver.find_elements_by_tag_name("a")
            # 6 links in nav bar and 6 on about page
            assert len(links) == 15       
        except StaleElementReferenceException: 
            pass  

    # def test_teams_text(self):
    #     self.driver.get(self.homeUrl + "/teams")
    #     self.driver.implicitly_wait(10)
    #     # checking different sections
    #     WebDriverWait(self.driver, 20).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div/div/div[1]")))
    #     try:
    #         section = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[1]"
    #         )[0].text
    #         print("SECTION : " + section)
    #         assert "Total teams:" in section
    #     except StaleElementReferenceException:
    #         section = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[1]"
    #         )[0].text
    #         assert "Total teams:" in section

    def test_teams_click_row(self):
        self.driver.get(self.homeUrl + "/teams")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[5]/table/tbody/tr[1]")))
        row1 = self.driver.find_element_by_xpath("/html/body/div/div/div[5]/table/tbody/tr[1]")
        ActionChains(self.driver).move_to_element(row1).click().perform()

    def test_teams_sort_table(self):
        self.driver.get(self.homeUrl + "/teams")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[4]/div[1]/div/div")))
        sort_button = self.driver.find_element_by_xpath("/html/body/div/div/div[4]/div[1]/div/div")
        ActionChains(self.driver).move_to_element(sort_button).click().perform()

    # def test_teams_pagination(self):
    #     self.driver.get(self.homeUrl + "/teams")
    #     self.driver.implicitly_wait(5)
    #     self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    #     # test going to page 2
    #     WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[2]/div/ul/li[4]")))
    #     page2 = self.driver.find_element_by_xpath("/html/body/div/div/div[2]/div/ul/li[4]")
    #     ActionChains(self.driver).move_to_element(page2).click().perform()
        # # test going to prev page
        # self.driver.find_elements_by_xpath(
        #     "/html/body/div/div/div[2]/div/ul/li[2]/a")[0].click()
        # # test going to next page
        # self.driver.find_elements_by_xpath(
        #     "/html/body/div/div/div[2]/div/ul/li[11]/a")[0].click()
        # # test going to first page
        # self.driver.find_elements_by_xpath(
        #     "/html/body/div/div/div[2]/div/ul/li[1]/a")[0].click()
        # # test going to last page
        # self.driver.find_elements_by_xpath(
        #     "/html/body/div/div/div[2]/div/ul/li[12]/a")[0].click()

    # def test_players_text(self):
    #     self.driver.get(self.homeUrl + "/teams")
    #     self.driver.implicitly_wait(10)
    #     try:
    #         section = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[1]")[0].text
    #         assert "Total players:" in section
    #     except StaleElementReferenceException:
    #         section = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[1]")[0].text
    #         assert "Total players:" in section

    def test_players_click_row(self):
        self.driver.get(self.homeUrl + "/players")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[5]/table/tbody/tr[1]")))
        row1 = self.driver.find_element_by_xpath("/html/body/div/div/div[5]/table/tbody/tr[1]")
        ActionChains(self.driver).move_to_element(row1).click().perform()

    def test_players_sort_table(self):
        self.driver.get(self.homeUrl + "/players")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[4]/div[1]/div/div")))
        row1 = self.driver.find_element_by_xpath("/html/body/div/div/div[4]/div[1]/div/div")
        ActionChains(self.driver).move_to_element(row1).click().perform()

    # def test_players_pagination(self):
    #     self.driver.get(self.homeUrl + "/players")
    #     self.driver.implicitly_wait(10)
    #     # test going to page 2
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[4]")[0].click()
    #     # test going to prev page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[2]/a")[0].click()
    #     # test going to next page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[11]/a")[0].click()
    #     # test going to first page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[1]/a/span[1]")[0].click()
    #     # test going to last page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[12]/a/span[1]")[0].click()

    # def test_matches_text(self):
    #     self.driver.get(self.homeUrl + "/matches")
    #     self.driver.implicitly_wait(10)
    #     try:
    #         section = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[1]"
    #         )[0].text
    #         assert "Total matches:" in section
    #     except StaleElementReferenceException:
    #         section = self.driver.find_elements_by_xpath(
    #             "/html/body/div/div/div[1]"
    #         )[0].text
    #         assert "Total matches:" in section

    def test_matches_click_row(self):
        self.driver.get(self.homeUrl + "/matches")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[5]/table/tbody/tr[1]")))
        row1 = self.driver.find_element_by_xpath("/html/body/div/div/div[5]/table/tbody/tr[1]")
        ActionChains(self.driver).move_to_element(row1).click().perform()
        
    def test_matches_sort_table(self):
        self.driver.get(self.homeUrl + "/matches")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div/div/div[4]/div[1]/div/div")))
        row1 = self.driver.find_element_by_xpath("/html/body/div/div/div[4]/div[1]/div/div")
        ActionChains(self.driver).move_to_element(row1).click().perform()
    
    # def test_matches_pagination(self):
    #     self.driver.get(self.homeUrl + "/matches")
    #     self.driver.implicitly_wait(10)
    #     # test going to page 2
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[4]")[0].click()
    #     self.driver.implicitly_wait(2)
    #     # test going to prev page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[2]")[0].click()
    #     self.driver.implicitly_wait(2)
    #     # test going to next page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[11]")[0].click()
    #     self.driver.implicitly_wait(2)
    #     # test going to first page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[1]")[0].click()
    #     self.driver.implicitly_wait(2)
    #     # test going to last page
    #     self.driver.find_elements_by_xpath(
    #         "/html/body/div/div/div[2]/div/ul/li[12]")[0].click()
    
    def test_basics(self):
        # go to basics page
        self.driver.get(self.homeUrl + "/basics")
        self.driver.implicitly_wait(10)
        assert self.homeUrl + "/basics" in self.driver.current_url
        # check different sections of the basics page
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[1]/h1"
            )[0].text
            assert section == "The Basics"
        except StaleElementReferenceException:
            pass
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[2]/div[1]/div/p[1]"
            )[0].text
            assert section == "Winning"
        except StaleElementReferenceException:
            pass
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[2]/div[2]/div/p[1]"
            )[0].text
            assert section == "Yellow and Red Cards"
        except StaleElementReferenceException:
            pass
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[2]/div[4]/div/p[1]"
            )[0].text
            assert section == "Handling"
        except StaleElementReferenceException:
            pass
        try:
            section = self.driver.find_elements_by_xpath(
                "/html/body/div/div/div[2]/div[5]/div/p[1]"
            )[0].text
            assert section == "Out of Bounds Plays"   
        except StaleElementReferenceException:
            pass

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()

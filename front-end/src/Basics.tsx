import React from 'react';
import "./basics.css"
import field from "./images/field.jpg";
import outOfBoundsPlays from "./images/outOfBoundsPlays.png";
import { Grid, Paper } from "@mui/material";

function Basics() {
  return (
    <div>
        <div className="Heading">
            <h1 className="TitleOne">
                The Basics</h1>
            <h2 className="TitleTwo">
                If you're new to football and want to brush up on the rules, you're in the right place!</h2>
            <img className = "HeaderPicture" src={field} alt = "Football field"/>
        </div>
        <div className="Text">
            <p className="Topic"></p>
            <p>Each match is played by two teams. Each team has 11 players, one of which is the goalkeeper.
                A match lasts 90 mintues, split up into two 45-minute halves.</p>
                <br/>
            <Grid container padding="3rem 0 2rem 0" justifyContent="center">
                <Paper className="paper" elevation={5} >
                    <p className="Topic">Winning</p>
                    <p className="Body">The object of the game is to get more goals than the other team. A team gets a goal
                        when the ball crosses the painted line on the ground in front of the net.
                        <br/><br/>When a game is tied, there will 
                        usually be two 15-minute halves of overtime. If the game is still tied after overtime, 
                        the game will go to penalty kicks. For penalty kicks, each team usually gets five shots 
                        with a different person on their team taking each shot, and the teams alternate shooting.
                        The goalkeeper stays the same throughout. Players take kicks from a spot in the middle
                        of the goal box and there are no defenders.<br/><br/>If after five shots the game 
                        is still tied, the game goes to "sudden death". This is where each team takes individual 
                        rounds of kicks, and the first round where one team misses and the other team makes it 
                        ends the game.     
                    </p>
                </Paper>
            </Grid>
            <Grid container padding="3rem 0 4rem 0" justifyContent="center">
                <Paper className="paper" elevation={5} >
                    <p className="Topic">Yellow and Red Cards</p>
                    <p className="Body">Players receive a yellow card for a bad foul, repeated infringement after one or
                        more warnings, or intentional handling of the ball. A red card can be given when
                        a player receives two yellow cards in the same game, or when a player intentionally
                        fouls to stop a goal scoring opportunity. Red cards are also given if an extremely
                        dangerous foul is committed. One example of actions that could be seen as worthy
                        of a yellow card could be slide tackling a player in a fashion that could injure
                        them or put them at risk of injury. Some examples of red cards may be striking another
                        player or intentionally handling the ball in order to prevent a goal by the opposing team.
                        Sometimes cards are also awarded for actions outside of the game, like threatening
                        a referee, leaving the field without permission, or starting a fight with another
                        player.
                    </p>
                    <br/>
                    <p className="Body">If a player receives a yellow card, they may continue playing. However, if they receive
                        a second yellow card in the same match, then they are awarded a red card and ejected.
                        Whenever a player receives a red card either immediately or through 2 yellows, they are
                        ejected from the game and their team may not substitute another player in for the ejected
                        player, essentially lowering the team size by 1, like from 11 to 10, for example. If 5
                        players receive a red card on the same team, the team is disqualified. 
                    </p>
                    <br/>
                    <p className="Body">One example of where the rules may get controversial is the 2010 FIFA World Cup, where
                        Luis Suarez, a player from Uruguay who was not the goalkeeper, intentionally prevented the opposing
                        team, Ghana, from scoring by using his hands to block the ball on the goal line. As a result,
                        he was given a red card and ejected, and Ghana was awarded a penalty kick. However, Ghana
                        missed and Uruguay eventually won the game in a penalty shootout at the end of the game.
                        Some people found the blatant prevention of a goal using one's hands as against the spirit
                        of the game, while others simply agreed that nothing that happened was technically against
                        the rules of the game. Below is a highlight which captures this moment in the game.
                    </p>
                </Paper>
            </Grid>
            <div className = "CenterContent"><iframe width="560" height="315" src="https://www.youtube.com/embed/5mvpS-yvij0?start=63"
                title="Suarez Handball Red Card" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen/></div>
            <Grid container padding="3rem 0 2rem 0" justifyContent="center">
                <Paper className="paper" elevation={5} >
                    <p className="Topic">Handling</p>
                    <p className="Body">Whether or not an action is considered handling can be 
                        tricky because it takes into account intent, distance from the body, and reaction time. 
                        In general, if the player uses his arm or hand to stop the ball, this is a hand ball. 
                        However, if the player's arm was in a natural position close to their body, this likely 
                        won't be considered a handball, especially if they did not have much time to react to 
                        the ball coming their way. In this case, a card may not be awarded.
                    </p>
                </Paper>
            </Grid>
            <Grid container padding="3rem 0 4rem 0" justifyContent="center">
                <Paper className="paper" elevation={5} >
                    <p className="Topic">Out of Bounds Plays</p>
                    <p className="Body">Depending on where the ball goes out of bounds, and off of which player, three different
                        events can occur. If the ball goes out on the long edges or sidelines, then the ball is
                        thrown into play by the opposite team of whoever sent the ball out. However, if the ball
                        goes out on the endline and the attacking team sent it out, then a goal kick occurs.
                        The ball is placed within the goalie box, and an opportunity to freely kick the ball
                        is given to any player on the defending team. If the defending team sends it out through
                        the endline, then a corner kick is awarded. The attacking team gets to place the ball on
                        either corner of the field connected to that endline, and are awarded an opportunity to
                        freely kick the ball. This usually results in a pass towards attacking players near the
                        opposing teams goal. Below is a diagram visualizing these ideas. 
                    </p>
                </Paper>
            </Grid>
            <div className = "CenterContent"><img src = {outOfBoundsPlays} alt = "Diagram of out of bounds plays" className = "BodyPicture" height = "16rem"/></div>
        </div>
    </div>
  );
}

export default Basics;
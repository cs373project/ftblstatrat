import React, { useState, useEffect } from 'react';
import "./App.css"
import { Link, useHistory } from "react-router-dom";
import { Button, ListGroup } from "react-bootstrap";
import { Radar } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import Loader from "./Loader";
import Api from "./Api";
import Teams from './Teams';


function PlayerInstance(props: any) {
  const history = useHistory();
  const [data, setData] = useState({} as any);
  const [loading, setLoading] = useState(true);
  const [recentMatches, setRecentMatches] = useState([] as any);
  const [team, setTeam] = useState({} as any);
  const [radarData, setRadarData] = useState({} as any)

  function handleTeamClick(e: any) {
    props.setTeamId(e.target.id);
  }

  function handleMatchClick(e: any) {
    props.setMatchId(e.target.id);
  }

  useEffect(() => {
    async function calculatePlayerStats() {
      setLoading(true);
      const playerData = await Api.getModelData("players", "players", {perPage: 4000}, undefined);
      let maxValues = {maxHeight: 0, maxWeight: 0, maxGoals: 0, maxGames: 0}
      if (playerData && data) {
        playerData.forEach((player: any) => {
          if (maxValues.maxHeight < parseInt(player.player_height)) {
            maxValues.maxHeight = parseInt(player.player_height);
          }
          if (maxValues.maxWeight < parseInt(player.player_weight)) {
            maxValues.maxWeight = parseInt(player.player_height);
          }
          if (player.player_goals && maxValues.maxGoals < player.player_goals) {
            maxValues.maxGoals = player.player_goals;
          }
          if (player.player_games && maxValues.maxGames < player.player_games) {
            maxValues.maxGames = player.player_games;
          }
        });
        setRadarData({
          labels: ["Height (cm)", "Weight (kg)", "Goals", 
          "Games"],
          datasets: [
            {
              label: data.player_name,
              data: [parseInt(data.player_height) / maxValues.maxHeight, parseInt(data.player_weight) / maxValues.maxWeight, 
                data.player_goals / maxValues.maxGoals, data.player_games / maxValues.maxGames],
              backgroundColor: "rgba(6, 156,51, .3)",
              borderColor: "#02b844",
            },
            {
              label: "Average Player",
              data: [0.5, 0.5, 0.5, 0.5],
              backgroundColor: "rgba(6, 60,160, .3)",
              borderColor: "#5493ff",
            },
            {
              label: "Max Values",
              data: [1, 1, 1, 1],
              backgroundColor: "rgba(160, 6,6, .3)",
              borderColor: "#ff554f",
            }
          ]
        });
      }
      
      setLoading(false);
    }

    calculatePlayerStats();
  }, [data])

  useEffect(() => {
    const getData = async () => {
      try {
				setLoading(true)
        const data = await Api.getModelData("players/id=" + props.id, undefined, {}, undefined);
        const matchPlayerData = await Api.getModelData("matches/players", "matchplayer ids", {}, undefined);
        const playerTeamData = await Api.getModelData("players/teams", "playerteam ids", {}, undefined);
        const teamData = await Api.getModelData("teams", "teams", {perPage: 4000}, undefined);
        const matchData = await Api.getModelData("matches", "matches", {perPage: 4000}, undefined);
        setData(data);
        if (data) {
          let recentMatchIds = [] as any;
          matchPlayerData.forEach((val: any) => val.players.split(" ").includes(data.player_id.toString()) ? recentMatchIds.push(val.match_id) : undefined);
          setRecentMatches(matchData.filter((val: any) => recentMatchIds.includes(val.match_id)));
          setTeam(teamData.find((val: any) => val.team_id.toString() === playerTeamData.find((otherVal: any) => otherVal.player_id === data.player_id).team_id));
        }
				
				setLoading(false)
			} catch (err) {
				console.error(err);
			}
    }
    getData();
  }, [history, props.id]);

  return ( loading ? <Loader /> : !data ? props.dataNotFound :
    <div className = "models-instance">
      <div className = "models-center-content"><img src = {data.player_photo} alt = {data.player_name}/></div>
      <div className = "models-center-list-group">
        <ListGroup horizontal>
          <ListGroup.Item>Name</ListGroup.Item>
          <ListGroup.Item>{data.player_name}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Age</ListGroup.Item>
          <ListGroup.Item>{data.player_age}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Height</ListGroup.Item>
          <ListGroup.Item>{data.player_height}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Weight</ListGroup.Item>
          <ListGroup.Item>{data.player_weight}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Position</ListGroup.Item>
          <ListGroup.Item>{data.player_position}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Team Name</ListGroup.Item>
          <ListGroup.Item>{!(data.player_teamName && data.player_teamName.includes("Tigres UANL")) && (<div><Link to="/teaminstance">
                      <Button variant = "link" id = { team && team.team_id } onClick = { handleTeamClick } >
                        {data.player_teamName}
                      </Button>
                    </Link><img className = "models-resize-team-logo" src = {team && team.team_logoLink} alt = {data.player_teamName}/></div>)}
          </ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Jersey Number</ListGroup.Item>
          <ListGroup.Item>{data.player_jerseyNumber}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Hometown</ListGroup.Item>
          <ListGroup.Item>{data.player_hometown}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Country</ListGroup.Item>
          <ListGroup.Item>{data.player_country}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Games</ListGroup.Item>
          <ListGroup.Item>{data.player_games}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Goals</ListGroup.Item>
          <ListGroup.Item>{data.player_goals}</ListGroup.Item>
        </ListGroup>
        <ListGroup horizontal>
          <ListGroup.Item>Recent games</ListGroup.Item>
          <ListGroup.Item>{
                  recentMatches.map((match: any) => 
                    <Link to="/matchinstance">
                      <Button variant = "link" id = { match.match_id } onClick = { handleMatchClick } >
                        {match.match_home_name} vs {match.match_away_name},  
                      </Button>
                    </Link>
                  )}
          </ListGroup.Item>
        </ListGroup>
      </div>
      <div className = "models-center-radar-header"><h1 className = "models-title">Player Percentiles</h1></div>
      <div className = "models-center-radar">
        <MDBContainer>
          <Radar data={radarData} className = "models-radar"/>
        </MDBContainer>
      </div>
    </div>
  );
}

export default PlayerInstance;
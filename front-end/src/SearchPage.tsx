import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Table, Pagination } from "react-bootstrap";
import Loader from "./Loader";
import {
	NumberParam,
	StringParam,
	useQueryParams,
	withDefault,
} from "use-query-params";
import Api from "./Api";
import { Button } from '@mui/material';
import Highlighter from "react-highlight-words";

function SearchPage(props: any) {
    const history = useHistory();
    const [searchParam, setSearchParam] = useState("");
    const [params, setParams] = useQueryParams({
      page: withDefault(NumberParam, 1),
      perPage: withDefault(NumberParam, 20),
      q: StringParam,
    })
    const [loading, setLoading] = useState(true);
    const [teamData, setTeamData] = useState([] as any);
    const [playerData, setPlayerData] = useState([] as any);
    const [matchData, setMatchData] = useState([] as any);
    const [playerTableItems, setPlayerTableItems] = useState([] as any);
    const [teamTableItems, setTeamTableItems] = useState([] as any);
    const [matchTableItems, setMatchTableItems] = useState([] as any);
  
    function handleSearch(e: any) {
        if (!e.key || e.key === "Enter") {
            setParams({...params, q: searchParam});
        }
    }
  
    function handleSearchInput(e: any) {
      setSearchParam(e.target.value);
    }

    function handlePlayerClick(e: any) {
        props.setPlayerId(e.target.id);
        history.push("/playerinstance/id=" + e.target.id);
    }

    function handleMatchClick(e: any) {
        props.setMatchId(e.target.id);
        history.push("/matchinstance/id=" + e.target.id);
    }

    function handleTeamClick(e: any) {
        props.setTeamId(e.target.id);
        history.push("/teaminstance/id=" + e.target.id);
    }
  
    function getStreak(record: any) {
        if (record.length === 0) {
          return undefined;
        }
        var recent = "";
        if (record.indexOf("W") === 0) {
          recent = "W";
        } else if (record.indexOf("L") === 0) {
          recent = "L";
        } else if (record.indexOf("D") === 0) {
          recent = "D";
        }
        var i = 2;
        while (i <= record.length) {
          if (record.indexOf(recent.repeat(i)) !== 0) {
            return `${recent}${i - 1}`;
          }
          i++;
        }
        return `${recent}${i}`;
      }

    useEffect(() => {
      const getData = async () => {
        const constructURLParams = (params: any) => {
            let URLParams = new URLSearchParams()
            URLParams.append("page", params.page)
            URLParams.append("perPage", params.perPage)
            if (params.q) {
                URLParams.append("q", params.q)
            }
            return URLParams
        }

        try {
            setLoading(true)
            let teamData;
            let playerData;
            let matchData;
            if (params.q) {
                teamData = await Api.getModelData("/teams?search&q=" + params.q + "&page=" + params.page + "&perPage=" + params.perPage, undefined, constructURLParams(params), undefined);
                playerData = await Api.getModelData("/players?search&q=" + params.q + "&page=" + params.page + "&perPage=" + params.perPage, undefined, constructURLParams(params), undefined);
                matchData = await Api.getModelData("/matches?search&q=" + params.q + "&page=" + params.page + "&perPage=" + params.perPage, undefined, constructURLParams(params), undefined);
                teamData = teamData["teams"]
                playerData = playerData["players"]
                matchData = matchData["matches"]
                setTeamData(teamData);
                setMatchData(matchData);
                setPlayerData(playerData);
                var simplifiedPlayerData = playerData.map((val: any) => {
                    var newData =
                    {
                      name: val.player_name,
                      country: val.player_country,
                      ageRange: val.player_age,
                      position: val.player_position,
                      goals: val.player_goals,
                      games: val.player_games,
                      id: val.player_id
                    };
                    return newData;
                  });
                
                var simplifiedTeamData = teamData.map((val: any) => {
                    var newData =
                    {
                        name: val.team_name,
                        league: val.team_league,
                        country: val.team_country,
                        founded: val.team_founded,
                        goals: val.team_goals,
                        penalty: val.team_penaltyPercentage,
                        streak: getStreak(val.team_form),
                        id: val.team_id
                    };
                    return newData;
                  });
                
                var simplifiedMatchData = matchData.map((val: any) => {
                    var newData =
                    {
                        name: val.match_home_name + " vs " + val.match_away_name,
                        league: val.match_league,
                        hometeam: val.match_home_name,
                        awayteam: val.match_away_name,
                        stadium: val.match_venue_name,
                        city: val.match_location,
                        id: val.match_id
                    };
                    return newData;
                  });
                setMatchTableItems(simplifiedMatchData);
                setTeamTableItems(simplifiedTeamData);
                setPlayerTableItems(simplifiedPlayerData);
            }
            setLoading(false)
        } catch (err) {
            console.error(err);
        }
    }
      getData();
    }, [history, params]);

    const playerDataFields = [{field: "name", title: "Match name"}, {field: "country", title: "Country"}, {field: "ageRange", title: "Ages"},
        {field: "position", title: "Position"}, {field: "goals", title: "Total Goals"}, {field: "games", title: "Games Played"}];
    const matchDataFields = [{field: "name", title: "Match name"}, {field: "league", title: "League"}, {field: "hometeam", title: "Home Team"},
        {field: "awayteam", title: "Away Team"}, {field: "stadium", title: "Stadium"}, {field: "city", title: "City"}];
    const teamDataFields = [{field: "name", title: "Team name"}, {field: "league", title: "League"}, {field: "country", title: "Country"},
        {field: "founded", title: "Year Founded"}, {field: "goals", title: "Total Goals"}, {field: "penalty", title: "Penalty %"}, {field: "streak", title: "Streak"}];

    function highlightResult(highlightText: string) {
        //  highlightClassName="YourHighlightClass"        
        return (
            <Highlighter
                searchWords={params.q ? params.q.split(" ") : []}
                autoEscape={true}
                textToHighlight={highlightText}
            />
        );
    }

    return ( loading ? <Loader /> :
        <div className = "search-main">
            <div className = "search-center-space-evenly">
                <div className="search-page-bar">
                    <input type="text" placeholder="Search " value={searchParam} onChange={handleSearchInput} onKeyDown={handleSearch}/>
                </div>
                <Button onClick={handleSearch} className = "search-button" variant="contained">Search</Button>
            </div>
            <div className="search-header">Players</div>
            {playerData.length === 0 ? <p className = "search-not-found">No players found</p> : 
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        {playerDataFields.map((line: any) => <th id = {line.field}>{line.title}</th>)}
                    </tr>
                    </thead>
                    <tbody>
                    {playerTableItems.map((val: any, index: any) =>
                        <tr key={index} className = "models-clickable">
                        {playerDataFields.map((line: any) => <td id = { val.id } onClick = {handlePlayerClick}>{typeof val[line.field] == "string" ? highlightResult(val[line.field]) : val[line.field]}</td>)}
                        </tr>)}
                    </tbody>
                </Table>
            }
            <div className="search-header">Matches</div>
            {matchData.length === 0 ? <p className = "search-not-found">No matches found</p> : 
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        {matchDataFields.map((line: any) => <th id = {line.field}>{line.title}</th>)}
                    </tr>
                    </thead>
                    <tbody>
                    {matchTableItems.map((val: any, index: any) =>
                        <tr key={index} className = "models-clickable">
                        {matchDataFields.map((line: any) => <td id = { val.id } onClick = {handleMatchClick}>{typeof val[line.field] == "string" ? highlightResult(val[line.field]) : val[line.field]}</td>)}
                        </tr>)}
                    </tbody>
                </Table>
            }
            <div className="search-header">Teams</div>
            {teamData.length === 0 ? <p className = "search-not-found">No teams found</p> : 
                <Table striped bordered hover>
                    <thead>
                    <tr>
                        {teamDataFields.map((line: any) => <th id = {line.field}>{line.title}</th>)}
                    </tr>
                    </thead>
                    <tbody>
                    {teamTableItems.map((val: any, index: any) =>
                        <tr key={index} className = "models-clickable">
                        {teamDataFields.map((line: any) => <td id = { val.id } onClick = {handleTeamClick}>{typeof val[line.field] == "string" ? highlightResult(val[line.field]) : val[line.field]}</td>)}
                        </tr>)}
                    </tbody>
                </Table>
            }
        </div>
    );
}

export default SearchPage;
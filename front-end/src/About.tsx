import {Component} from 'react';
import "./basics.css";
import kelly from './images/kelly.jpg';
import nitin from './images/nitin.jpg';
import amer from './images/amer.jpg';
import erica from './images/erica.jpg';
import bo from './images/bo.png';
import { Card } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import aws from './images/aws.jpg';
import axios from './images/axios.png';
import flask from './images/flask.png';
import gitlab from './images/gitlab.png';
import namecheap from './images/namecheap.png';
import plantuml from './images/plantuml.png';
import postman from './images/postman.png';
import react from './images/react.png';
import selenium from './images/selenium.png';
import sql from './images/sql.jpg';

class About extends Component {

    state = {
        commits: [],
        issues: [],
    }

    componentDidMount() {
        
        fetch('https://gitlab.com/api/v4/projects/29886344/repository/commits?all&per_page=100')
        .then(res => res.json())
        .then((data) => {
          this.setState({ commits: data })
        })
        .catch(console.log)

        fetch('https://gitlab.com/api/v4/projects/29886344/issues?scope=all&per_page=100')
        .then(res => res.json())
        .then((data1) => {
          this.setState({ issues: data1 })
        })
        .catch(console.log)

    }


    render() {

        let numIssuesBo = 0
        let numIssuesNitin = 0
        let numIssuesErica = 0
        let numIssuesKelly = 0
        let numIssuesAmer = 0

        for (let issueNum in this.state.issues) {
            let issue = this.state.issues[issueNum]
            let assignees = (issue as any).assignees

            for (let assigneeNum in assignees) {
                let assignee = assignees[assigneeNum]

                if (assignee.name === "Bo-Deng") {
                    numIssuesBo++
                }
                if (assignee.name === "Nitin Jain") {
                    numIssuesNitin++
                }
                if (assignee.name === "Erica Xu") {
                    numIssuesErica++
                }
                if (assignee.name === "Kelly Sun") {
                    numIssuesKelly++
                }
                if (assignee.name === "Amer Jusupovic") {
                    numIssuesAmer++
                }
            }
        }

        const teamData = [
            {
              img: amer,
              title: "Amer Jusupovic",
              description: "Hey everyone! My name is Amer Jusupovic and I grew up in Richardson, Texas before moving to Plano when I was 7. I attended Liberty High School in Frisco. My favorite thing to do outside of school are basketball, volleyball, and video games. This passion for video games led to an interest in coding up my own game, eventually leading me to computer science at UT Austin.",
              responsibility: "Responsibility: Frontend",
              commits: `Commits: ${this.state.commits.filter(c => (c as any).author_name === "Amer Jusupovic").length}`,
              issues:  `Issues: ${numIssuesAmer}`,
              tests: "Unit tests: 0"
            },
            {
              img: nitin,
              title: "Nitin Jain",
              description: "I was born in Manchester, New Hampshire then moved to Austin, TX. I am majoring in CS because I love the amount of creativity it offers and is a field where it will be very hard for me to become bored in. Technology and problem solving are also areas which I have been interested in since the beginning of high school.",
              responsibility: "Responsibility: Backend",
              commits: `Commits: ${this.state.commits.filter(c => (c as any).author_name === "Nitin Jain").length}`,
              issues:  `Issues: ${numIssuesNitin}`,
              tests: "Unit tests: 21"
            },
            {
              img: kelly,
              title: "Kelly Sun",
              description: "My name is Kelly Sun and I’m from Plano, Texas. I attended Liberty High School, where I spent most of my time playing cello in the orchestra. In my free time I like reading and watching sitcoms! Although I took some computer science classes in high school, I never thought that I would be majoring in CS until it was time to apply to colleges and I still didn’t know what I wanted to major in. I decided to give CS another try and ended up really liking it!",
              responsibility: "Responsibility: Frontend",
              commits: `Commits: ${this.state.commits.filter(c => (c as any).author_name === "Kelly Sun").length}`,
              issues:  `Issues: ${numIssuesKelly}`,
              tests: "Unit tests: 15"
            }
          ];

          // this function was inspired by 
          // https://stackoverflow.com/questions/66905176/how-to-redirect-on-click-to-external-url-react and
          // https://stackoverflow.com/questions/51977823/type-void-is-not-assignable-to-type-event-mouseeventhtmlinputelement?rq=1
          const handleCardClick = (url:string) => {
            return (event: React.MouseEvent) => {
                window.location.href = url
                event.preventDefault();
              }
          }

          const teamData2 = [
            {
              img: erica,
              title: "Erica Xu",
              description: "I grew up in Houston, TX after moving there when I was 7 years old and attended Clear Creek High School. I came into college undeclared and transferred into the CS program after deciding to give it another chance because I liked how versatile it is. I wanted to major in something that would allow me to problem solve, be creative, and combine my hobby for art.",
              responsibility: "Responsibility: Frontend",
              commits: `Commits: ${this.state.commits.filter(c => (c as any).author_name === "Erica Xu").length}`,
              issues:  `Issues: ${numIssuesErica}`,
              tests: "Unit tests: 10"
            },
            {
              img: bo,
              title: "Bo Deng",
              description: "I was born in Tianjin, China and moved to Austin, Texas when I was four years old. I’ve been here ever since! I’m majoring in Computer Science because I love problem-solving and finding new ways to tackle the most interesting challenges that our world faces. I really enjoy programming, but I also really enjoy thinking about designing new systems or approaches in which I can apply my understanding of computer science to solve meaningful problems.",
              responsibility: "Responsibility: Frontend",
              commits: `Commits: ${this.state.commits.filter(c => (c as any).author_name === "Bo Deng").length}`,
              issues:  `Issues: ${numIssuesBo}`,
              tests: "Unit tests: 10"
            }
          ];

          const tools = [
            {
                img: aws,
                title: "AWS",
                description: "Where we're hosting our website",
                link: "https://aws.amazon.com/websites/"
            },
            {
                img: axios,
                title: "Axios",
                description: "Used to scrape the APIs",
                link: "https://www.npmjs.com/package/axios"
            },
            {
                img: flask,
                title: "Flask",
                description: "Web framework for the backend server",
                link: "https://flask.palletsprojects.com/en/2.0.x/"
            },
            {
                img: gitlab,
                title: "Gitlab",
                description: "Helped us with version control and a clear workflow",
                link: "https://gitlab.com/"
            },
            {
                img: namecheap,
                title: "Namecheap",
                description: "Where we registered our domain name",
                link: "https://www.namecheap.com/"
            }
        ];

        const tools2 = [
            {
                img: plantuml,
                title: "PlantUML",
                description: "Used to document and visually represent the system behind the website",
                link: "https://plantuml.com/"
            },
            {
                img: postman,
                title: "Postman",
                description: "Used for API documentation",
                link: "https://www.postman.com/"
            },
            {
                img: react,
                title: "React",
                description: "Used for front-end development",
                link: "https://reactjs.org/"
            },
            {
                img: selenium,
                title: "Selenium",
                description: "Used to make acceptance tests of the user interface",
                link: "https://www.selenium.dev/"
            },
            {
                img: sql,
                title: "SQLAlchemy",
                description: "Web framework for the backend server",
                link: "https://www.sqlalchemy.org/"
            }
        ];
        return (
            <div>
            <div className="Heading">
                <h1 className="TitleOne">About Us</h1>
            </div>
                <p className="Text">
                    Football Stat Rat is an app designed for soccer fans. It allows users to find stats 
                    about teams, individual players, and matches, all in one place. This is a helpful resource 
                    for those looking to learn more about their professional teams and connect with fans in 
                    their community. If you don’t know much about soccer and are looking to become more 
                    aware of its cultural significance, we'll help you get started.
                    <br/><br/>
                    By compiling information from the various datasets that we've used to build the app, 
                    fans will be able to answer interesting questions like "Which teams have the most player 
                    turnover?" and gain additional insight into their favorite players and teams.
                    <br></br><br></br>
                    We've already learned a lot from integrating disparate data. For example, 4-3-3 seems to
                    be the most common formation!
                </p>
                <br/>
                <h2 className="TitleTwo" style={{textAlign:"center"}}>The Team</h2>

                <div className = "CardContainerAbout">{teamData.map((val: any) => 
                    <Card key = {val.title} className = "AboutCard">
                    <Card.Img variant = "top" src = {val.img} id = {val.route} className = "CardImageTeam"/>
                    <Card.Body id = {val.route} className = "">
                        <Card.Title id = {val.route} className = "CardTitle">{val.title}</Card.Title>
                        <Card.Text id = {val.route} className = "CardBody">{val.description}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.responsibility}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.commits}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.issues}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.tests}</Card.Text>
                    </Card.Body>
                    </Card>
                    )}</div>
                <div className = "CardContainer2About">{teamData2.map((val: any) => 
                    <Card key = {val.title} className = "AboutCard">
                    <Card.Img variant = "top" src = {val.img} id = {val.route} className = "CardImageTeam"/>
                    <Card.Body id = {val.route} className = "">
                        <Card.Title id = {val.route} className = "CardTitle">{val.title}</Card.Title>
                        <Card.Text id = {val.route} className = "CardBody">{val.description}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.responsibility}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.commits}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.issues}</Card.Text>
                        <Card.Text id = {val.route} className = "CardStats">{val.tests}</Card.Text>
                    </Card.Body>
                    </Card>
                    )}</div>
                <br/>
                <h2 className = "TitleTwo" style={{textAlign:"center"}}>Project Stats</h2>
                <div className = "Text">
                <p>Total commits: {this.state.commits.length}</p>
                <p>Total issues: {this.state.issues.length}</p>
                <p>Total unit tests: 56</p>
                </div>
                <h2 className = "TitleTwo" style={{textAlign:"center"}}>APIs and Datasets</h2>
                <ul className = "Text">
                  <p>These were scraped with axios calls in javascript and http connections in python.</p>
                  <li><a href = "https://www.api-football.com/">API-Football</a></li>
                  <li><a href = "https://boggio-analytics.com/">Boggio Analytics</a></li>
                  <li><a href = "https://rapidapi.com/scorebat/api/free-football-soccer-videos/">
                    RapidAPI Soccer Videos</a></li>
                  <li><a href = "https://developers.google.com/youtube/v3">
                    Youtube API</a></li>
                </ul>
                <h2 className = "TitleTwo" style={{textAlign:"center"}}>Tools Used</h2>
                
                {/* <ul className = "Text">
                  <li><a href = "https://reactjs.org/">React</a>: Used for front-end development</li>
                  <li><a href = "https://www.postman.com/">Postman</a>: Used for API documentation</li>
                  <li><a href = "https://gitlab.com/">Git</a>: Helped us with version control and a clear workflow</li>
                  <li><a href = "https://www.namecheap.com/">Namecheap</a>: Where we registered our domain name</li>
                  <li><a href = "https://aws.amazon.com/websites/">AWS</a>: Where we're hosting our website</li>
                  <li><a href = "https://www.npmjs.com/package/axios">Axios</a>: Used to scrape the APIs</li>
                  <li><a href = "https://flask.palletsprojects.com/en/2.0.x/">Flask</a> and <a href = "https://www.sqlalchemy.org/">SQLAlchemy</a>: Web framework for the backend server</li>
                  <li><a href = "https://www.selenium.dev/">Selenium</a>: Used to make acceptance tests of the user interface</li>
                  <li><a href = "https://plantuml.com/">PlantUML</a>: Used to document and visually represent the system behind the website</li>
                </ul> */}

                <div className = "CardContainerAbout">{tools.map((val: any) => 
                    <Card key = {val.title} className = "AboutCard">
                    <Card.Img variant = "top" src = {val.img} onClick = {handleCardClick(val.link)} id = {val.route} className = "CardImageTool"/>
                    <Card.Body onClick = {handleCardClick(val.link)} id = {val.route} className = "">
                        <Card.Title onClick = {handleCardClick(val.link)} id = {val.route} className = "CardTitle">{val.title}</Card.Title>
                        <Card.Text onClick = {handleCardClick(val.link)} id = {val.route} className = "CardBody">{val.description}</Card.Text>
                    </Card.Body>
                    </Card>
                )}</div>
                <div className = "CardContainerAbout">{tools2.map((val: any) => 
                    <Card key = {val.title} className = "AboutCard">
                    <Card.Img variant = "top" src = {val.img} onClick = {handleCardClick(val.link)} id = {val.route} className = "CardImageTool"/>
                    <Card.Body onClick = {handleCardClick(val.link)} id = {val.route} className = "">
                        <Card.Title onClick = {handleCardClick(val.link)} id = {val.route} className = "CardTitle">{val.title}</Card.Title>
                        <Card.Text onClick = {handleCardClick(val.link)} id = {val.route} className = "CardBody">{val.description}</Card.Text>
                    </Card.Body>
                    </Card>
                )}</div>

                <h2 className = "TitleTwo" style={{textAlign:"center"}}>Links</h2>
                <ul className = "Text">
                  <li><a href = "https://gitlab.com/cs373project/ftblstatrat">
                    GitLab Repo (ftblstatrat)</a></li>
                  <li><a href = "https://documenter.getpostman.com/view/17673920/UUy3A7CS">
                    Postman API Documentation</a></li>
                </ul>
              </div>
        );
    }
}

export default About;
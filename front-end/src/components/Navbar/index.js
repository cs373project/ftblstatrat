/*inspired by code from: https://www.geeksforgeeks.org/create-a-responsive-navbar-using-reactjs/*/
import React from 'react';
import {
	Nav,
	NavLink,
	Bars,
	NavMenu,
} from './NavbarElements';

const Navbar = () => {
	return (
		<>
			<Nav class="sticky">
				<Bars />
				<NavMenu>
					<NavLink to='/' activeStyle>
						Home
					</NavLink>
					<NavLink to='/about' activeStyle>
						About
					</NavLink>
					<NavLink to='/teams' activeStyle>
						Teams
					</NavLink>
					<NavLink to='/players' activeStyle>
						Players
					</NavLink>
					<NavLink to='/matches' activeStyle>
						Matches
					</NavLink>
					<NavLink to='/basics' activeStyle>
						The Basics
					</NavLink>
					<NavLink to='/highlights' activeStyle>
						Notable Matches
					</NavLink>
					<NavLink to='/visualizations' activeStyle>
						Visualizations
					</NavLink>
					<NavLink to='/providerVisualizations' activeStyle>
						Provider Visualizations
					</NavLink>
				</NavMenu>
			</Nav>
		</>
	);
};

export default Navbar;

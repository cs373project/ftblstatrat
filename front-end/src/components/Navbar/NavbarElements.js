/*inspired by code from https://stackoverflow.com/questions/58096450/how-to-have-a-sticky-navbar-in-reactjs
and https://www.geeksforgeeks.org/create-a-responsive-navbar-using-reactjs/*/
import { FaBars } from 'react-icons/fa';
import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';

export const Nav = styled.nav`
background: #5e3096;
position: fixed;
display: flex;
justify-content: flex-start;
align-items: center;
overflow: hidden;
z-index: 1;
width: 100%;
margin: auto;
top: 0;
border-bottom: solid 1px var(--primary-color);
opacity: 0.9;
position: fixed;
top: 0;
`;

export const NavLink = styled(Link)`
display: flex;
height: 100%;
align-items: center;
text-decoration: none;
font-size: 15px;
font-family: "Gill Sans", sans-serif;
text-transform: uppercase;
color: #FFFFFF;
padding: 1rem 2rem;
&.active {
    color: white;
}
:hover {
  background-color: #4f2270;
  color: #ffffff;
  cursor: pointer;
`;

export const Bars = styled(FaBars)`
display: none;
color: #808080;
color: #ffffff;
@media screen and (max-width: 768px) {
    display: block;
}
`;

export const NavMenu = styled.div`
display: flex;
align-items: center;
margin-right: -24px;
@media screen and (max-width: 768px) {
    display: none;
}
`;

/* inspired by code from 
https://www.newline.co/@andreeamaco/react-dropdown-tutorial-for-beginners-create-a-dropdown-menu-from-scratch--9831d197 */

import React from "react";
import styled from "styled-components";

const DropdownWrapper = styled.form`
  display: flex;
  flex-flow: column;
  justify-content: flex-start;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`;

const StyledSelect = styled.select`
  max-width: 75rem;
  height: 100%;
  padding: 0.5rem;
  margin-bottom: 1rem;
`;

const StyledOption = styled.option`
  color: ${(props) => (props.selected ? "lightgrey" : "black")};
`;

const StyledLabel = styled.label`
  margin-bottom: 1rem;
  margin-left: 0.5rem;
  margin-right: 0.5rem;
`;

export function Dropdown(props:any) {
  return (
    <DropdownWrapper action={props.action}>
      <StyledLabel htmlFor="services">
        {props.formLabel}
      </StyledLabel>
      <StyledSelect id="services" name="services">
        {props.children}
      </StyledSelect>
    </DropdownWrapper>
  );
}

export function Option(props:any) {
  return (
    <StyledOption selected={props.selected}>
      {props.value}
    </StyledOption>
  );
}
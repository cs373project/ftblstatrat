import { dividerClasses } from '@mui/material';
import {Component} from 'react';
import "./basics.css";
import PlayerInstance from './PlayerInstance';
import { CitiesIncomeChart, ContaminantsChart, StateSitesChart } from './visualizations/provider';

class ProviderVisualizations extends Component {

    render() {
        return (
            <div>
                <div className="Heading">
                    <h1 className="TitleOne">Provider Visualizations</h1>
                </div>
                <StateSitesChart></StateSitesChart>
                <CitiesIncomeChart></CitiesIncomeChart>
                <ContaminantsChart></ContaminantsChart>
            </div>
        );
    }
}

export default ProviderVisualizations;
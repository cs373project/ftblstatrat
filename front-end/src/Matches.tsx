import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Table, Pagination } from "react-bootstrap";
import { BsFillTriangleFill } from "react-icons/bs";
import Loader from "./Loader";
import {
	NumberParam,
	StringParam,
	useQueryParams,
	withDefault,
} from "use-query-params";
import Api from "./Api";
import { InputLabel, MenuItem, FormControl, Select, Button, TextField } from '@mui/material';
import Highlighter from "react-highlight-words";

function Matches(props: any) {
  const dataFields = [{field: "name", title: "Match name"}, {field: "league", title: "League"}, {field: "hometeam", title: "Home Team"},
  {field: "awayteam", title: "Away Team"}, {field: "stadium", title: "Stadium"}, {field: "city", title: "City"}];
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(20);
  const [count, setCount] = useState(0);
  const [searchParam, setSearchParam] = useState("");

  const history = useHistory();
  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 20),
    league: StringParam,
    hometeam: StringParam,
    awayteam: StringParam,
    sort: StringParam, // withDefault(StringParam, "city"),
    q: StringParam,
  })

  const addPage = count % params.perPage === 0 ? 0 : 1;
  const NUM_PAGES = Math.floor(count / params.perPage) + addPage;

  function handleClick(e: any) {
    props.setId(e.target.id);
    history.push("/matchinstance/id=" + e.target.id);
  }

  function handlePageClick(e: any) {
    if (e.target.id >= 1 && e.target.id <= NUM_PAGES) {
      setParams({...params, page: Number(e.target.id)});
    }
  }

  function handleFilterClick(e: any, index: any) {
    if (index.props.id === "league") {
      setParams({...params, league: e.target.value, page: 1})
    } else if (index.props.id === "hometeam") {
      setParams({...params, hometeam: e.target.value, page: 1})
    } else if (index.props.id === "awayteam") {
      setParams({...params, awayteam: e.target.value, page: 1})
    } else if (index.props.id === "perPage") {
      setParams({...params, perPage: e.target.value, page: 1})
    }
  }

  function handleSortClick(e: any, index: any) {
    if (index.props.id === "stadium") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "-stadium") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "city") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "-city") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "none") {
      setParams({...params, sort: undefined, page: 1})
    }
  }

  function handleSearch(e: any) {
    if (!e.key || e.key === "Enter") {
      setParams({...params, q: searchParam});
    }
  }

  function handleSearchInput(e: any) {
    setSearchParam(e.target.value);
  }

  const [tableItems, setTableItems] = useState([] as any);

  // based on TexasVotes group: https://github.com/forbesye/texasvotes/blob/master/back-end/District.py
  useEffect(() => {
    const getData = async () => {
      const constructURLParams = (params: any) => {
				let URLParams = new URLSearchParams()
				URLParams.append("page", params.page)
        URLParams.append("perPage", params.perPage)
        if (params.q) {
					URLParams.append("q", params.q)
				}
				if (params.league) {
					URLParams.append("league", params.league)
				}
        if (params.hometeam) {
					URLParams.append("hometeam", params.hometeam)
				}
        if (params.awayteam) {
					URLParams.append("awayteam", params.awayteam)
				}
        if (params.sort) {
          URLParams.append("sort", params.sort)
        }
				return URLParams
			}

      try {
				setLoading(true)
        let data;
        if (params.q) {
          data = await Api.getModelData("matches?search", undefined, constructURLParams(params), undefined);
        } else {
          data = await Api.getModelData("matches", undefined, constructURLParams(params), undefined);
        }
        setCount(data["count"]);
        data = data["matches"];
        
        var simplifiedData = data.map((val: any) => {
          var newData =
          {
            name: val.match_home_name + " vs " + val.match_away_name,
            league: val.match_league,
            hometeam: val.match_home_name,
            awayteam: val.match_away_name,
            stadium: val.match_venue_name,
            city: val.match_location,
            id: val.match_id
          };
          return newData;
        });
				setTotal(simplifiedData.length)
				setTableItems(simplifiedData)
				setLoading(false)
			} catch (err) {
				console.error(err);
			}
    }
    getData();
  }, [count, history, params]);

  function createPages() {
    const MIN_PAGE_COLLAPSE = 4;
    const addPage = count % params.perPage === 0 ? 0 : 1;
    const NUM_PAGES = Math.floor(count / params.perPage) + addPage;
    const MAX_PAGE_COLLAPSE = NUM_PAGES - 2;
    const MID_PAGE_RANGE = 4;
    
    let items = [];
    let start = params.page >= MIN_PAGE_COLLAPSE ? params.page - (MID_PAGE_RANGE / 2) : 2;
    items.push(<Pagination.First className = "models-page-ends" onClick={handlePageClick} id={"1"} disabled = {params.page === 1}/>,
    <Pagination.Prev className = "models-page-ends" onClick={handlePageClick} id={(params.page - 1).toString()} disabled = {params.page === 1}/>);
    if (count > 0) {
      items.push(
        <Pagination.Item className = "models-page-item" onClick={handlePageClick} id={"1"} activeLabel={''} active={params.page === 1}>1</Pagination.Item>);
    }
    if (start >= MIN_PAGE_COLLAPSE - 1) {
      items.push(<Pagination.Ellipsis className = "models-page-ends" disabled/>);
    }
    for (let number = start; number <= NUM_PAGES && number <= start + MID_PAGE_RANGE; number++) {
      items.push(
        <Pagination.Item className = "models-page-item" onClick={handlePageClick} id={number.toString()} key={number} activeLabel={''} active={params.page === number}>{number}</Pagination.Item>);
    }
    let end = start + MID_PAGE_RANGE > NUM_PAGES ? NUM_PAGES : start + MID_PAGE_RANGE;
    if (end <= MAX_PAGE_COLLAPSE) {
      items.push(<Pagination.Ellipsis className = "models-page-ends" disabled/>);
    }
    if (end === NUM_PAGES - 1 || end <= MAX_PAGE_COLLAPSE) {
      items.push(
        <Pagination.Item className = "models-page-item" onClick={handlePageClick} id={NUM_PAGES.toString()} activeLabel={''} active={params.page === NUM_PAGES}>{NUM_PAGES}</Pagination.Item>);
    }
    items.push(<Pagination.Next className = "models-page-ends" onClick={handlePageClick} id={(params.page + 1).toString()} disabled = {params.page === NUM_PAGES}/>,
    <Pagination.Last className = "models-page-ends" onClick={handlePageClick} id={(NUM_PAGES).toString()} disabled = {params.page === NUM_PAGES}/>);
    document.querySelectorAll('[aria-hidden="true"]').forEach((child) => child.addEventListener("click", handlePageClick));
    return items;
  }

  let leagues = ["Bundesliga 1", "Eredivisie", "La Liga", "Liga MX", "Ligue 1", "Major League Soccer", "Premier League", "Primiera Liga", "Serie A", "Super League"]
  let hometeams = ["Orlando City SC", "Estac Troyes", "Real Madrid", "Borussia Dortmund", "Waalwijk", "Houston Dynamo", "Guangzhou Evergrande FC", "Cagliari",
    "Paris Saint Germain", "Beijing Guoan", "Colorado Rapids", "Sassuolo", "Shenzhen Ruby FC", "Pacos Ferreira", "Rennes", "FC Juarez", "Rayo Vallecano", "Nashville SC",
    "Borussia Monchengladbach", "Vitesse", "Angers", "Puebla", "Watford", "VfL Wolfsburg", "Reims", "Seattle Sounders", "NEC Nijmegen", "Saint Etienne", "Metz",
    "Stade Brestois 29", "Eintracht Frankfurt", "Bordeaux", "Sevilla", "Heracles", "Espanyol", "Venezia", "AZ Alkmaar", "Willem II", "Sporting CP", "Nantes", "Strasbourg",
    "New York Red Bulls", "Burnley", "Monaco", "Henan Jianye", "New York City FC", "Wolves", "Genoa", "Mazatlán", "Norwich", "Real Sociedad", "Bayer Leverkusen", "Real Salt Lake",
    "Club Tijuana", "Udinese", "1899 Hoffenheim", "Lyon", "Alaves", "Celta Vigo", "Chicago Fire", "Manchester City", "PEC Zwolle", "Philadelphia Union", "Lens", "Atlas",
    "Montreal Impact", "Leeds", "SHANGHAI SIPG", "Guimaraes", "FC Augsburg", "Club America", "Marseille", "Los Angeles FC", "Cambuur", "Tianjin Teda", "Heerenveen",
    "Arminia Bielefeld", "Torino", "Benfica", "Moreirense", "Inter Miami", "New England Revolution", "Groningen", "Guadalajara Chivas", "Cadiz", "Atlanta United FC", "Lazio",
    "Los Angeles Galaxy", "Clermont Foot", "Lorient", "U.N.A.M. - Pumas", "Empoli", "Inter", "Montpellier", "Atalanta", "Belenenses", "GIL VicenteGIL Vicente", "Barcelona",
    "Toronto FCToronto FC", "Sampdoria", "Villarreal", "Shanghai Shenhua", "Everton", "Arouca", "FC Cincinnati", "Union Berlin", "San Jose Earthquakes", "Arsenal", "Brighton",
    "AC Milan", "VfB Stuttgart", "Lille", "FC Porto", "Changchun Yatai", "Maritimo", "Tottenham", "VfL BOCHUM", "SC Freiburg", "Bologna", "Fortuna Sittard", "Liverpool",
    "Chongqing Lifan", "Salernitana", "Pachuca", "Crystal Palace", "Guangzhou R&F", "Qingdao Huanghai", "FC Koln", "Athletic Club", "Portimonense", "Ajax", "Sparta Rotterdam",
    "SC Braga", "Dalian Aerbin", "Leicester", "Utrecht", "DC United", "Spezia", "Chelsea", "Shandong Luneng", "Cruz Azul", "Levante", "Nice", "Vancouver Whitecaps", "Southampton",
    "Mallorca", "Aston Villa", "Santos Laguna", "Manchester United", "GO Ahead Eagles", "Newcastle", "FSV Mainz 05", "SpVgg Greuther Furth", "Bayern Munich", "Brentford",
    "PSV Eindhoven", "Santa Clara", "Tondela", "Club Queretaro", "Leon", "Monterrey", "RB Leipzig", "Verona", "Minnesota United FC", "Austin", "Feyenoord", "Valencia",
    "Hertha Berlin", "Portland Timbers", "Juventus", "AS Roma", "Hebei Zhongji", "Fiorentina", "Shijiazhuang Y. J."]
  let awayteams = ["Orlando City SC", "Estac Troyes", "Real Madrid", "Borussia Dortmund", "Waalwijk", "Houston Dynamo", "Guangzhou Evergrande FC", "Cagliari",
    "Paris Saint Germain", "Beijing Guoan", "Colorado Rapids", "Sassuolo", "Shenzhen Ruby FC", "Pacos Ferreira", "Rennes", "FC Juarez", "Rayo Vallecano", "Nashville SC",
    "Borussia Monchengladbach", "Vitesse", "Angers", "Puebla", "Watford", "VfL Wolfsburg", "Reims", "Seattle Sounders", "NEC Nijmegen", "Saint Etienne", "Metz",
    "Stade Brestois 29", "Eintracht Frankfurt", "Bordeaux", "Sevilla", "Heracles", "Espanyol", "Venezia", "AZ Alkmaar", "Willem II", "Sporting CP", "Nantes", "Strasbourg",
    "New York Red Bulls", "Burnley", "Monaco", "Henan Jianye", "New York City FC", "Wolves", "Genoa", "Mazatlán", "Norwich", "Real Sociedad", "Bayer Leverkusen", "Real Salt Lake",
    "Club Tijuana", "Udinese", "1899 Hoffenheim", "Lyon", "Alaves", "Celta Vigo", "Chicago Fire", "Manchester City", "PEC Zwolle", "Philadelphia Union", "Lens", "Atlas",
    "Montreal Impact", "Leeds", "SHANGHAI SIPG", "Guimaraes", "FC Augsburg", "Club America", "Marseille", "Los Angeles FC", "Cambuur", "Tianjin Teda", "Heerenveen",
    "Arminia Bielefeld", "Torino", "Benfica", "Moreirense", "Inter Miami", "New England Revolution", "Groningen", "Guadalajara Chivas", "Cadiz", "Atlanta United FC", "Lazio",
    "Los Angeles Galaxy", "Clermont Foot", "Lorient", "U.N.A.M. - Pumas", "Empoli", "Inter", "Montpellier", "Atalanta", "Belenenses", "GIL VicenteGIL Vicente", "Barcelona",
    "Toronto FCToronto FC", "Sampdoria", "Villarreal", "Shanghai Shenhua", "Everton", "Arouca", "FC Cincinnati", "Union Berlin", "San Jose Earthquakes", "Arsenal", "Brighton",
    "AC Milan", "VfB Stuttgart", "Lille", "FC Porto", "Changchun Yatai", "Maritimo", "Tottenham", "VfL BOCHUM", "SC Freiburg", "Bologna", "Fortuna Sittard", "Liverpool",
    "Chongqing Lifan", "Salernitana", "Pachuca", "Crystal Palace", "Guangzhou R&F", "Qingdao Huanghai", "FC Koln", "Athletic Club", "Portimonense", "Ajax", "Sparta Rotterdam",
    "SC Braga", "Dalian Aerbin", "Leicester", "Utrecht", "DC United", "Spezia", "Chelsea", "Shandong Luneng", "Cruz Azul", "Levante", "Nice", "Vancouver Whitecaps", "Southampton",
    "Mallorca", "Aston Villa", "Santos Laguna", "Manchester United", "GO Ahead Eagles", "Newcastle", "FSV Mainz 05", "SpVgg Greuther Furth", "Bayern Munich", "Brentford",
    "PSV Eindhoven", "Santa Clara", "Tondela", "Club Queretaro", "Leon", "Monterrey", "RB Leipzig", "Verona", "Minnesota United FC", "Austin", "Feyenoord", "Valencia",
    "Hertha Berlin", "Portland Timbers", "Juventus", "AS Roma", "Hebei Zhongji", "Fiorentina", "Shijiazhuang Y. J."]
  let sort = ["City: A - Z", "City: Z - A", "Venue: A - Z", "Venue: Z - A"]

  function highlightResult(highlightText: string) {
    //  highlightClassName="YourHighlightClass"        
    return (
        <Highlighter
            searchWords={params.q ? params.q.split(" ") : []}
            autoEscape={true}
            textToHighlight={highlightText}
        />
    );
  }

  return (
    <div>
      <div className="Heading">
          <h1 className="TitleOne">Matches</h1>
      </div>
      <div className = "models-center-content">Matches in page: {total}, Total matches: {count}</div>
      <div className = "models-center-space-evenly">
        <div className="search-bar">
          <TextField className = "search-field" type="text" placeholder="Search " value={searchParam} onChange={handleSearchInput} onKeyDown={handleSearch} variant="outlined"/>
        </div>
        <div className = "search-button-wrapper">
          <Button onClick={handleSearch} className = "search-button" variant="contained">Search</Button>
        </div>
      </div>
      <div className="dropdowns">
        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >League</InputLabel>
          <Select
            id="league"
            value={"Choose option"}
            renderValue={() => params.league}
            label="League"
            onChange={handleFilterClick}
            autoWidth
          >
            <MenuItem id="league" value={undefined}>None</MenuItem>
            {leagues.map(
              (val: any) => <MenuItem key={val} id="league" value={val}>{val}</MenuItem>
            )}
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Home Team</InputLabel>
          <Select
            id="hometeam"
            value={"Choose option"}
            renderValue={() => params.hometeam}
            label="Home Team"
            onChange={handleFilterClick}
            autoWidth
          >
            <MenuItem id="hometeam" value={undefined}>None</MenuItem>
            {hometeams.map(
              (val: any) => <MenuItem key={val} id="hometeam" value={val}>{val}</MenuItem>
            )}
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Away Team</InputLabel>
          <Select
            id="awayteam"
            value={"Choose option"}
            renderValue={() => params.awayteam}
            label="Away Team"
            onChange={handleFilterClick}
            autoWidth
          >
            <MenuItem id="awayteam" value={undefined}>None</MenuItem>
            {awayteams.map(
              (val: any) => <MenuItem key={val} id="awayteam" value={val}>{val}</MenuItem>
            )}
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Sort by</InputLabel>
          <Select
            id="sort"
            value={"Choose option"}
            renderValue={() => params.sort}
            label="Sort by"
            onChange={handleSortClick}
            autoWidth
          >
            <MenuItem id="none" value={undefined}>None</MenuItem>
            <MenuItem id="stadium" value={"Venue: A - Z"}>{"Venue: A - Z"}</MenuItem>
            <MenuItem id="-stadium" value={"Venue: Z - A"}>{"Venue: Z - A"}</MenuItem>
            <MenuItem id="city" value={"City: A - Z"}>{"City: A - Z"}</MenuItem>
            <MenuItem id="-city" value={"City: Z - A"}>{"City: Z - A"}</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className="models-table">
        {loading ? <Loader /> :
          <Table striped bordered hover>
            <thead>
              <tr>
                {dataFields.map((line: any) => <th id = {line.field}>{line.title} {params.sort === line.field && <BsFillTriangleFill/>}</th>)}
              </tr>
            </thead>
            <tbody>
              {tableItems.map((val: any, index: any) =>
                <tr key={index} className = "models-clickable">
                  {dataFields.map((line: any) => <td id = { val.id } onClick = {handleClick}>{typeof val[line.field] == "string" ? highlightResult(val[line.field]) : (val[line.field])}</td>)}
                </tr>)}
            </tbody>
          </Table>
        }
        <div className = "models-center-pagination">
          <Pagination>{createPages()}</Pagination>
          <FormControl sx={{ m: 1, minWidth: "10rem" }}>
            <InputLabel >Entries per page</InputLabel>
            <Select
              id="perPage"
              value={"Choose option"}
              renderValue={() => params.perPage}
              label="Entries per page"
              onChange={handleFilterClick}
              autoWidth
            >
              <MenuItem id="perPage" value={20}>20</MenuItem>
              <MenuItem id="perPage" value={40}>40</MenuItem>
              <MenuItem id="perPage" value={60}>60</MenuItem>
              <MenuItem id="perPage" value={100}>100</MenuItem>
            </Select>
          </FormControl>
        </div>
      </div>
    </div>
  );
}

export default Matches;



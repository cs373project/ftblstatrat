import axios from "axios";
import React, { useEffect, useState } from "react"
import {
	NumberParam,
	StringParam,
	useQueryParams,
	withDefault,
} from "use-query-params";
import {
	Bar,
	BarChart,
	CartesianGrid,
    Cell,
	Legend,
	XAxis,
	YAxis,
	Tooltip,
	Radar,
	RadarChart,
    Pie,
    PieChart,
	PolarGrid,
	PolarAngleAxis,
	PolarRadiusAxis,
	Label,
    RadialBarChart,
    RadialBar,
	ResponsiveContainer,
    Scatter,
    ScatterChart
} from "recharts"
import { Typography } from "antd"
import {Loader} from "../Loader"
import styles from "./visualizations.css"

const { Title } = Typography

const API_URL = "https://api.chemicalsnear.me/"

async function getModelData(endpoint, index, params, callback) {
    let data = {};
    data = await axios({
        method: 'get',
        url: API_URL + "/" + endpoint,
        params: params
    })
    .then(function (response) {
        let data_val = response.data;
        return data_val;
    })
    .catch(function (error) {
        console.log(error);
        return {};
    });
    if (index) {
        return data[index];
    } 
    return data;
}

const StateSitesChart = () => {
    const [data, setData] = useState(new Map())
    const [chartData, setChartData] = useState([]);
    const [loading, setLoading] = useState(true);

    const [params, setParams] = useQueryParams({
		page: withDefault(NumberParam, -1),
	})
    
    useEffect(() => {
        const getData = async () => {
            setLoading(true)
            
            let superfundsData = await getModelData("superfunds", undefined, params, undefined);
            superfundsData = superfundsData["superfunds"]

            superfundsData.forEach(superfund => {
                const state = superfund.state
                // increment this state in the map
                if (!data.has(state)) {
                    data.set(state, 0);
                }
                data.set(state, data.get(state) + 1);
            });

            for (const [key, value] of data.entries()) {
                chartData.push({"state": key, "numSites": value})
            }

            chartData.sort((a,b) => (a.state > b.state) ? 1 : ((b.name > a.name) ? -1 : 0))

            console.log(chartData)
            
            setLoading(false);
        }
        getData()

    }, []);

    if (loading) {
		return <></>
	}
  
    return (
      <div>
          <Title level={3} style={{marginLeft:"5rem"}}>Superfund Sites by State</Title>
            <div style={{"margin":"auto", "width":"75%"}}>
                <ResponsiveContainer width="100%" aspect={2}>
                    <BarChart data={chartData} margin={{ top: 0, right: 0, bottom: 250, left: 0 }}>
                        <CartesianGrid />
                        <XAxis dataKey="state" angle={-90} interval={0} dx = {-5} dy={100}/>
                        <YAxis>
                            <Label value="Number of Superfund Sites" angle={-90} position="insideLeft" style={{textAnchor:"middle"}} />
                        </YAxis>
                        <Bar dataKey="numSites" fill="#8884d8"/>
                        <Tooltip></Tooltip>
                    </BarChart>
                </ResponsiveContainer>
            </div>
      </div>
    );
};

const CitiesIncomeChart = () => {
    const [chartData, setChartData] = useState([]);
    const [loading, setLoading] = useState(true);

    const [params, setParams] = useQueryParams({
		page: withDefault(NumberParam, -1),
	})
    
    useEffect(() => {
        const getData = async () => {
            setLoading(true)
            
            let citiesData = await getModelData("cities", undefined, params, undefined);
            citiesData = citiesData["cities"]

            citiesData.forEach(city => {
                const income = city.median_household_income
                const insurance = city.no_health_insurance
                
                chartData.push({"income": income, "insurance": insurance})
            });

            console.log(chartData)
            
            setLoading(false);
        }
        getData()

    }, []);

    if (loading) {
		return <></>
	}
  
    return (
      <div>
          <Title level={3} style={{marginLeft:"5rem"}}>Household Income vs Insurance Percentage</Title>
            <div style={{"margin":"auto", "width":"75%"}}>
                <ResponsiveContainer width="100%" aspect={2}>
                <ScatterChart
                    width={400}
                    height={400}
                    margin={{
                        top: 20,
                        right: 20,
                        bottom: 100,
                        left: 20,
                    }}
                >
                    <CartesianGrid />
                    <XAxis type="number" dataKey="income" name="Median Household Income" unit=" USD">
                        <Label value="Median Household Income" offset={0} position="insideBottom" style={{textAnchor:"middle"}} dy={20}/>
                    </XAxis>
                    <YAxis type="number" dataKey="insurance" name="Percentage of People Without Insurance">
                        <Label value="Percentage of People Without Insurance" angle={-90} position="insideLeft" style={{textAnchor:"middle"}} />
                    </ YAxis>
                    <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                    <Scatter name="Cities Income Chart" data={chartData} fill="#8884d8" />
                </ScatterChart>
                </ResponsiveContainer>
            </div>
      </div>
    );
};

const ContaminantsChart = () => {
    const [data, setData] = useState(new Map())
    const [chartData, setChartData] = useState([])
	const [loading, setLoading] = useState(false)
    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, -1),
    })

    const meltingPointBuckets = [[-200, -100], [-100, 0], [0, 100], [100, 200], [200, 1000]]
    meltingPointBuckets.forEach(bucket => {
        const bucketLow = bucket[0]
        const bucketHigh = bucket[1]
        const bucketString = bucketLow + "ºC - " + bucketHigh + "ºC";

        data[bucketString] = 0;
    });


    useEffect(() => {
        const getData = async () => {
            setLoading(true)

            let contaminantsData = await getModelData("contaminants", undefined, params, undefined);
            contaminantsData = contaminantsData["contaminants"]

            contaminantsData.forEach(contaminant => {
                const melting = parseFloat(contaminant.melting_point)
                // console.log(melting)

                meltingPointBuckets.forEach(bucket => {
                    const bucketLow = bucket[0]
                    const bucketHigh = bucket[1]

                    if (melting >= bucketLow && melting < bucketHigh) {
                        const bucketString = bucketLow + "ºC - " + bucketHigh + "ºC";
                        console.log(bucketString)
                        console.log(melting)
                        console.log(data[bucketString])

                        data[bucketString] += 1;
                    }
                });

            });

            // console.log(data);
            let i = 0;
            const colors = ['#8884d8', '#83a6ed', '#8dd1e1', '#82ca9d', '#a4de6c']
            meltingPointBuckets.forEach(bucket => {
                const bucketLow = bucket[0]
                const bucketHigh = bucket[1]
                const bucketString = bucketLow + "ºC - " + bucketHigh + "ºC";


                chartData.push({"name": bucketString, "numContaminants": data[bucketString], "fill": colors[i]});
                i++;
            });

            console.log(chartData);

            setLoading(false);
        }
        getData()

    }, []);

    if (loading) {
		return <></>
	}

    return (
        <>
            <Title level={3} style={{marginLeft:"5rem"}}>Melting Point of Contaminants</Title>
            <div style={{"margin":"auto", "width":"90%"}}>
                <ResponsiveContainer width="100%" aspect={2}>
                <RadialBarChart 
                    width={730} 
                    height={250} 
                    innerRadius="10%" 
                    outerRadius="80%" 
                    data={chartData} 
                    startAngle={180} 
                    endAngle={0}
                >
                    <RadialBar minAngle={15} label={{ fill: '#666', position: 'insideStart' }} background clockWise={true} dataKey='numContaminants' />
                    <Legend iconSize={10} width={240} height={140} layout='vertical' verticalAlign='middle' align="right" />
                </RadialBarChart>
                </ResponsiveContainer>
            </div>
        </>

    )
}


export { StateSitesChart }
export { CitiesIncomeChart }
export { ContaminantsChart }
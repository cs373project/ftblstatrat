import React, { useEffect, useState } from "react"
import {
	NumberParam,
	StringParam,
	useQueryParams,
	withDefault,
} from "use-query-params";
import Api from "../Api";
import {
	Bar,
	BarChart,
	CartesianGrid,
    Cell,
	Legend,
	XAxis,
	YAxis,
	Tooltip,
	Radar,
	RadarChart,
    Pie,
    PieChart,
	PolarGrid,
	PolarAngleAxis,
	PolarRadiusAxis,
	Label,
	ResponsiveContainer,
} from "recharts"
import { Typography } from "antd"
import {Loader} from "../Loader"
import styles from "./visualizations.css"

const { Title } = Typography

const TeamsChart = () => {
    const [data, setData] = useState(new Map())
    const [chartData, setChartData] = useState([])
	const [loading, setLoading] = useState(false)

    const [params, setParams] = useQueryParams({
		page: withDefault(NumberParam, 1),
        perPage: withDefault(NumberParam, 200),
        league: StringParam,
        country: StringParam,
        yearRange: StringParam,
        sort: StringParam,
        q: StringParam,
	})

    useEffect(() => {
        // get all of the teams (list of jsons)
        // count up how many teams each country has
            // look at country field in each team
            // increment on the map

            const getData = async () => {
                setLoading(true)
                
                let teamsData = await Api.getModelData("teams", undefined, params, undefined);
                teamsData = teamsData["teams"]

                teamsData.forEach(team => {
                    const teamCountry = team.team_country
                    // increment this country in the map
                    if (!data.has(teamCountry)) {
                        data.set(teamCountry, 0);
                    }
                    data.set(teamCountry, data.get(teamCountry) + 1);
                });

                for (const [key, value] of data.entries()) {
                    chartData.push({"country": key, "numTeams": value})
                }

                console.log(chartData)
                
                setLoading(false);
            }
            getData()

    }, []);

    if (loading) {
		return <></>
	}

    return (
        <>
            <Title level={3} style={{marginLeft:"5rem"}}>Teams per Country</Title>
            {/* <div className={styles.chart}> */}
            <ResponsiveContainer width="100%" aspect={3}>
                <RadarChart cx="50%" cy="50%" outerRadius="80%" data={chartData}>
                    <PolarGrid />
                    <PolarAngleAxis dataKey="country" />
                    <PolarRadiusAxis angle={0} domain={[0, 30]}/>
                    <Radar name="Teams" dataKey="numTeams" stroke="#39A96B" fill="#39A96B" fillOpacity={0.6} />
                    <Tooltip></Tooltip>
                </RadarChart>
            </ResponsiveContainer>
        </>
    )
}

const MatchChart = () => {
    const [data, setData] = useState(new Map())
    const [chartData, setChartData] = useState([])
	const [loading, setLoading] = useState(false)

    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        perPage: withDefault(NumberParam, 300),
        league: StringParam,
        hometeam: StringParam,
        awayteam: StringParam,
        sort: StringParam, // withDefault(StringParam, "city"),
        q: StringParam,
    })

    useEffect(() => {
            const getData = async () => {
                setLoading(true)
                
                let matchesData = await Api.getModelData("matches", undefined, params, undefined); 
                matchesData = matchesData["matches"];

                matchesData.forEach(match => {
                    const matchLeague = match.match_league;
                    const matchScore = match.match_score;
                    const scoreArr = matchScore.split(" ");
                    const totalScore = parseInt(scoreArr[0]) + parseInt(scoreArr[2]);

                    // increment this league in the corresponding scoreMap
                    if (!data.has(totalScore)) {
                        data.set(totalScore, new Map());
                    }
                    let scoreMap = data.get(totalScore)
                    if (!scoreMap.has(matchLeague)) {
                        scoreMap.set(matchLeague, 0)
                    }
                    scoreMap.set(matchLeague, scoreMap.get(matchLeague) + 1);
                });
                console.log(data);

                for (const [key, leagueMap] of data.entries()) {
                    let barData = {"totalScore": key}
                    for (const [leagueName, frequency] of leagueMap.entries()) {
                        barData[leagueName] = frequency
                    }
                    chartData.push(barData)
                }

                chartData.sort((a,b) => (a.totalScore > b.totalScore) ? 1 : ((b.totalScore > a.totalScore) ? -1 : 0))

                console.log(chartData)
                
                setLoading(false);
            }
            getData()

    }, []);

    if (loading) {
		return <></>
	}

    return (
        <>
            <Title level={3} style={{marginLeft:"5rem"}}>Match Scores</Title>
            <div style={{"margin":"auto", "width":"50%"}}>
                <ResponsiveContainer width="100%" aspect={2}>
                    <BarChart data={chartData} >
                        <CartesianGrid />
                        <XAxis dataKey="totalScore">
                            <Label value="Total Score" offset={0} position="insideBottom" style={{textAnchor:"middle"}}/>
                        </XAxis>
                        <YAxis>
                            <Label value="Number of Matches" angle={-90} position="insideLeft" style={{textAnchor:"middle"}} />
                        </YAxis>
                        <Legend />
                        <Tooltip></Tooltip>
                        <Bar dataKey="Bundesliga 1" stackId="a" fill="#0A2F51" />
                        <Bar dataKey="Eredivisie" stackId="a" fill="#0E4D64" />
                        <Bar dataKey="La Liga" stackId="a" fill="#137177" />
                        <Bar dataKey="Liga MX" stackId="a" fill="#188977" />
                        <Bar dataKey="Ligue 1" stackId="a" fill="#1D9A6C" />
                        <Bar dataKey="Major League Soccer" stackId="a" fill="#39A96B" />
                        <Bar dataKey="Premier League" stackId="a" fill="#56B870" />
                        <Bar dataKey="Primiera Liga" stackId="a" fill="#74C67A" />
                        <Bar dataKey="Serie A" stackId="a" fill="#99D492" />
                        <Bar dataKey="Super League" stackId="a" fill="#BFE1B0" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        </>

    )
}

const PlayersChart = () => {
    const [data, setData] = useState(new Map())
    const [chartData, setChartData] = useState([])
	const [loading, setLoading] = useState(false)

    const [params, setParams] = useQueryParams({
        page: withDefault(NumberParam, 1),
        perPage: withDefault(NumberParam, 300),
        country: StringParam,
        ageRange: StringParam,
        position: StringParam,
        sort: StringParam, // withDefault(StringParam, "goals"),
        q: StringParam,
    })

    const COLORS = [
        "#0A2F51",
        "#0C3958",
        "#0D445F",
        "#0E5066",
        "#105D6D",
        "#126A74",
        "#13787A",
        "#15817A",
        "#178778",
        "#198D75",
        "#1B9471",
        '#1D9A6C',
        '#2CA26B',
        "#3CAB6B",
        "#4CB36D",
        "#5CBB71",
        '#6CC276',
        "#7CCA7E",
        "#92D18C",
        "#A8D99D",
        "#BCE0AD",
        "#CEE7BE",
        "#DEEDCF"
    ];

    useEffect(() => {
            const getData = async () => {
                setLoading(true)
                
                let playersData = await Api.getModelData("players", undefined, params, undefined);
                playersData = playersData["players"]

                
                playersData.forEach(player => {
                    const age = player.player_age
                    // increment this age in the map
                    if (!data.has(age)) {
                        data.set(age, 0);
                    }
                    data.set(age, data.get(age) + 1);
                });

                for (const [key, value] of data.entries()) {
                    chartData.push({"name": key, "numPlayers": value})
                }

                chartData.sort((a,b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))

                console.log(chartData)
                
                setLoading(false);
            }
            getData()

    }, []);

    if (loading) {
		return <></>
	}

    return (
        <>
            <Title level={3} style={{marginLeft:"5rem"}}>Player Ages</Title>
            {/* <div className={styles.chart}> */}
            <ResponsiveContainer width="100%" aspect={3}>
                <PieChart width={700} height={700}>
                    <Pie data={chartData} dataKey="numPlayers" outerRadius={200} fill="green">
                        {
                        chartData.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)
                        }
                    </Pie>
                    <Tooltip />
                    <Legend />
                </PieChart>
            </ResponsiveContainer>
        </>
    )
    
}



export { TeamsChart }
export { MatchChart }
export { PlayersChart }
import React, { useState, useEffect } from 'react';
import { useHistory } from "react-router-dom";
import { Table, Pagination } from "react-bootstrap";
import { BsFillTriangleFill } from "react-icons/bs";
import Loader from "./Loader";
import {
	NumberParam,
	StringParam,
	useQueryParams,
	withDefault,
} from "use-query-params";
import Api from "./Api";
import { InputLabel, MenuItem, FormControl, Select, Button, TextField } from '@mui/material';
import Highlighter from "react-highlight-words";

function Players(props: any) {
  const dataFields = [{field: "name", title: "Match name"}, {field: "country", title: "Country"}, {field: "ageRange", title: "Ages"},
  {field: "position", title: "Position"}, {field: "goals", title: "Total Goals"}, {field: "games", title: "Games Played"}];
  const [loading, setLoading] = useState(true);
  const [total, setTotal] = useState(20);
  const [count, setCount] = useState(0);
  const [searchParam, setSearchParam] = useState("");

  const history = useHistory();
  const [params, setParams] = useQueryParams({
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 20),
    country: StringParam,
    ageRange: StringParam,
    position: StringParam,
    sort: StringParam, // withDefault(StringParam, "goals"),
    q: StringParam,
  })

  const addPage = count % params.perPage === 0 ? 0 : 1;
  const NUM_PAGES = Math.floor(count / params.perPage) + addPage;

  function handleClick(e: any) {
    props.setId(e.target.id);
    history.push("/playerinstance/id=" + e.target.id);
  }

  function handlePageClick(e: any) {
    if (e.target.id >= 1 && e.target.id <= NUM_PAGES) {
      setParams({...params, page: Number(e.target.id)});
    }
  }

  function handleFilterClick(e: any, index: any) {
    if (index.props.id === "ageRange") {
      setParams({...params, ageRange: e.target.value, page: 1})
    } else if (index.props.id === "country") {
      setParams({...params, country: e.target.value, page: 1})
    } else if (index.props.id === "position") {
      setParams({...params, position: e.target.value, page: 1})
    } else if (index.props.id === "perPage") {
      setParams({...params, perPage: e.target.value, page: 1})
    }
  }

  function handleSortClick(e: any, index: any) {
    if (index.props.id === "goals") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "-goals") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "gamesplayed") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "-gamesplayed") {
      setParams({...params, sort: index.props.id, page: 1})
    } else if (index.props.id === "none") {
      setParams({...params, sort: undefined, page: 1})
    }
  }

  function handleSearch(e: any) {
    if (!e.key || e.key === "Enter") {
      setParams({...params, q: searchParam});
    }
  }

  function handleSearchInput(e: any) {
    setSearchParam(e.target.value);
  }

  const [tableItems, setTableItems] = useState([] as any);

  // based on TexasVotes group: https://github.com/forbesye/texasvotes/blob/master/back-end/District.py
  useEffect(() => {
    const getData = async () => {
      const constructURLParams = (params: any) => {
				let URLParams = new URLSearchParams()
				URLParams.append("page", params.page)
        URLParams.append("perPage", params.perPage)
        if (params.q) {
					URLParams.append("q", params.q)
				}
				if (params.country) {
					URLParams.append("country", params.country)
				}
        if (params.ageRange) {
					URLParams.append("ageRange", params.ageRange)
				}
        if (params.position) {
					URLParams.append("position", params.position)
				}
        if (params.sort) {
          URLParams.append("sort", params.sort)
        }
				return URLParams
			}

      try {
				setLoading(true)
        let data;
        if (params.q) {
          data = await Api.getModelData("players?search", undefined, constructURLParams(params), undefined);
        } else {
          data = await Api.getModelData("players", undefined, constructURLParams(params), undefined);
        }
        setCount(data["count"]);
        data = data["players"];
        
        var simplifiedData = data.map((val: any) => {
          var newData =
          {
            name: val.player_name,
            country: val.player_country,
            ageRange: val.player_age,
            position: val.player_position,
            goals: val.player_goals,
            games: val.player_games,
            id: val.player_id
          };
          return newData;
        });
				setTotal(simplifiedData.length)
				setTableItems(simplifiedData)
				setLoading(false)
			} catch (err) {
				console.error(err);
			}
    }
    getData();
  }, [count, history, params]);

  function createPages() {
    const MIN_PAGE_COLLAPSE = 4;
    const addPage = count % params.perPage === 0 ? 0 : 1;
    const NUM_PAGES = Math.floor(count / params.perPage) + addPage;
    const MAX_PAGE_COLLAPSE = NUM_PAGES - 2;
    const MID_PAGE_RANGE = 4;
    
    let items = [];
    let start = params.page >= MIN_PAGE_COLLAPSE ? params.page - (MID_PAGE_RANGE / 2) : 2;
    items.push(<Pagination.First className = "models-page-ends" onClick={handlePageClick} id={"1"} disabled = {params.page === 1}/>,
    <Pagination.Prev className = "models-page-ends" onClick={handlePageClick} id={(params.page - 1).toString()} disabled = {params.page === 1}/>);
    if (count > 0) {
      items.push(
        <Pagination.Item className = "models-page-item" onClick={handlePageClick} id={"1"} activeLabel={''} active={params.page === 1}>1</Pagination.Item>);
    }
    if (start >= MIN_PAGE_COLLAPSE - 1) {
      items.push(<Pagination.Ellipsis className = "models-page-ends" disabled/>);
    }
    for (let number = start; number <= NUM_PAGES && number <= start + MID_PAGE_RANGE; number++) {
      items.push(
        <Pagination.Item className = "models-page-item" onClick={handlePageClick} id={number.toString()} key={number} activeLabel={''} active={params.page === number}>{number}</Pagination.Item>);
    }
    let end = start + MID_PAGE_RANGE > NUM_PAGES ? NUM_PAGES : start + MID_PAGE_RANGE;
    if (end <= MAX_PAGE_COLLAPSE) {
      items.push(<Pagination.Ellipsis className = "models-page-ends" disabled/>);
    }
    if (end === NUM_PAGES - 1 || end <= MAX_PAGE_COLLAPSE) {
      items.push(
        <Pagination.Item className = "models-page-item" onClick={handlePageClick} id={NUM_PAGES.toString()} activeLabel={''} active={params.page === NUM_PAGES}>{NUM_PAGES}</Pagination.Item>);
    }
    items.push(<Pagination.Next className = "models-page-ends" onClick={handlePageClick} id={(params.page + 1).toString()} disabled = {params.page === NUM_PAGES}/>,
    <Pagination.Last className = "models-page-ends" onClick={handlePageClick} id={(NUM_PAGES).toString()} disabled = {params.page === NUM_PAGES}/>);
    document.querySelectorAll('[aria-hidden="true"]').forEach((child) => child.addEventListener("click", handlePageClick));
    return items;
  }

  let countries = ["Albania", "Algeria", "Andorra", "Angola", "Argentina", "Armenia", "Australia", 
    "Austria", "Belgium", "Bosnia and Herzegovina", "Brazil", "Brunei Darussalam", "Bulgaria", "Burkina Faso", 
    "Cameroon", "Canada", "Cape Verde Islands", "Chile", "China PR", "Chinese Taipei", "Colombia", "Congo DR", 
    "Congo", "Costa Rica", "Côte d'Ivoire", "Croatia", "Cuba", "Czech Republic", "Denmark", "Dominican Republic", 
    "Ecuador", "Egypt", "England", "Equatorial Guinea", "Estonia", "Ethopia", "Finland", "France", 
    "French Guiana", "Gabon", "Gambia", "Germany", "Ghana", "Greece", "Guadeloupe", "Guinea", "Guinea-Bissau", 
    "Honduras", "Hong Kong", "Hungary", "Iceland", "Indonesia", "Iran", "Isle of Man", "Israel", "Italy", 
    "Jamaica", "Japan", "Kenya", "Korea Republic", "Kosovo", "Latvia", "Lebanon", "Liberia", "Lithuania", 
    "Luxembourg", "Madagascar", "Mali", "Martinique", "Mayotte", "Mexico", "Moldova", "Morocco", "Mozambique", 
    "Netherlands", "New Zealand", "Nigeria", "North Macedonia", "Northern Ireland", "Norway", "Panama", 
    "Paraguay", "Peru", "Poland", "Portugal", "Republic of Ireland", "Romania", "Russia", "Senegal", "Serbia", 
    "Sierra Leon", "Slovakia", "Slovenia", "South Africa", "Spain", "Sweden", "Switzerland", "Syria", 
    "Thailand", "Togo", "Tunisia", "Turkey", "Uganda", "Ukraine", "Uruguay", "USA", "Uzbekistan", "Venezuela", 
    "Wales", "Yugoslavia", "Zambia", "Zimbabwe"];
  let positions = ["Attacker", "Defender", "Midfielder", "Goalkeeper"];
  let age = ["15-19", "20-24", "25-29", "30-34", "35-39", "40"];
  let sort = ["Total goals: Ascending", "Total goals: Descending", "Total games: Ascending", 
    "Total games: Descending"];

  function highlightResult(highlightText: string) {
    //  highlightClassName="YourHighlightClass"        
    return (
        <Highlighter
            searchWords={params.q ? params.q.split(" ") : []}
            autoEscape={true}
            textToHighlight={highlightText}
        />
    );
  }

  return (
    <div>
      <div className="Heading">
          <h1 className="TitleOne">Players</h1>
      </div>
      <div className="models-center-content">Players in page: {total}, Total players: {count}</div>
      <div className = "models-center-space-evenly">
        <div className="search-bar">
          <TextField className = "search-field" type="text" placeholder="Search " value={searchParam} onChange={handleSearchInput} onKeyDown={handleSearch} variant="outlined"/>
        </div>
        <div className = "search-button-wrapper">
          <Button onClick={handleSearch} className = "search-button" variant="contained">Search</Button>
        </div>
      </div>
      <div className="dropdowns">
        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Age Range</InputLabel>
          <Select
            id="ageRange"
            value={"Choose option"}
            renderValue={() => params.ageRange}
            label="Age Range"
            onChange={handleFilterClick}
            autoWidth
          >
            <MenuItem id="ageRange" value={undefined}>None</MenuItem>
            {age.map(
              (val: any) => <MenuItem key={val} id="ageRange" value={val}>{val}</MenuItem>
            )}
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Country</InputLabel>
          <Select
            id="country"
            value={"Choose option"}
            renderValue={() => params.country}
            label="Country"
            onChange={handleFilterClick}
            autoWidth
          >
            <MenuItem id="country" value={undefined}>None</MenuItem>
            {countries.map(
              (val: any) => <MenuItem key={val} id="country" value={val}>{val}</MenuItem>
            )}
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Position</InputLabel>
          <Select
            id="position"
            value={"Choose option"}
            renderValue={() => params.position}
            label="Position"
            onChange={handleFilterClick}
            autoWidth
          >
            <MenuItem id="position" value={undefined}>None</MenuItem>
            {positions.map(
              (val: any) => <MenuItem key={val} id="position" value={val}>{val}</MenuItem>
            )}
          </Select>
        </FormControl>

        <FormControl sx={{ m: 1, minWidth: "10rem" }}>
          <InputLabel >Sort by</InputLabel>
          <Select
            id="sort"
            value={"Choose option"}
            renderValue={() => params.sort}
            label="Sort by"
            onChange={handleSortClick}
            autoWidth
          >
            <MenuItem id="none" value={undefined}>None</MenuItem>
            <MenuItem id="goals" value={"Total goals: Ascending"}>{"Total goals: Ascending"}</MenuItem>
            <MenuItem id="-goals" value={"Total goals: Descending"}>{"Total goals: Descending"}</MenuItem>
            <MenuItem id="gamesplayed" value={"Total games: Ascending"}>{"Total games: Ascending"}</MenuItem>
            <MenuItem id="-gamesplayed" value={"Total games: Descending"}>{"Total games: Descending"}</MenuItem>
          </Select>
        </FormControl>
        
      </div>
      <div className="models-table">
        {loading ? <Loader /> :
          <Table striped bordered hover>
            <thead>
              <tr>
                {dataFields.map((line: any) => <th id = {line.field}>{line.title} {params.sort === line.field && <BsFillTriangleFill/>}</th>)}
              </tr>
            </thead>
            <tbody>
              {tableItems.map((val: any, index: any) =>
                <tr key={index} className = "models-clickable">
                  {dataFields.map((line: any) => <td id = { val.id } onClick = {handleClick}>{typeof val[line.field] == "string" ? highlightResult(val[line.field]) : val[line.field]}</td>)}
                </tr>)}
            </tbody>
          </Table>
        }
        <div className = "models-center-pagination">
          <Pagination>{createPages()}</Pagination>
          <FormControl sx={{ m: 1, minWidth: "10rem" }}>
            <InputLabel >Entries per page</InputLabel>
            <Select
              id="perPage"
              value={"Choose option"}
              renderValue={() => params.perPage}
              label="Entries per page"
              onChange={handleFilterClick}
              autoWidth
            >
              <MenuItem id="perPage" value={20}>20</MenuItem>
              <MenuItem id="perPage" value={40}>40</MenuItem>
              <MenuItem id="perPage" value={60}>60</MenuItem>
              <MenuItem id="perPage" value={100}>100</MenuItem>
            </Select>
          </FormControl>
        </div>
      </div>
    </div>
  );
}

export default Players;

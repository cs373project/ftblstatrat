import { initializeApp } from "firebase/app";
import { getFirestore, collection, addDoc, getDocs, /* doc, deleteDoc */ } from "firebase/firestore";
import axios from "axios";

// find way to save apikeys with later database implementation
// must replace this to see website with data
const firebaseConfig = {
  apiKey: "AIzaSyAbQtL_AR23DSm4tdkki6WnIOmLFuBPqfw",
  authDomain: "footballstatrat.firebaseapp.com",
  projectId: "footballstatrat",
  storageBucket: "footballstatrat.appspot.com",
  messagingSenderId: "690550866444",
  appId: "1:690550866444:web:325af3aa76a48f5b576429",
  measurementId: "G-N80F52ZZLG"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

const db= getFirestore();

// TODO: add way to save api key safely, environment variables still can be unsafe
const OTHER_API_KEY = 'x';

const teams = [1]; // maybe try test throttling calls? *sleep* between each call and see, added async and awaits too

function getTeamQueriesFunc() {
  const getTeamQueries = [{season: 2021,league: 39}, {season: 2021,league: 61}, {season: 2021,league: 140}, {season: 2021,league: 253}, {season: 2021,league: 78}
    , {season: 2021,league: 135}, {season: 2021,league: 262}, {season: 2021,league: 88}, {season: 2021,league: 169}, {season: 2021,league: 94}]

  getTeamQueries.forEach((param: any) => 
      axios({
          method: 'get',
          url: 'https://v3.football.api-sports.io/teams',
          headers: {
              'x-rapidapi-key': OTHER_API_KEY,
              'x-rapidapi-host': 'v3.football.api-sports.io'
          },
          params: param,
          },)
          .then(function (response: any) {
            const data = response.data.response;
            console.log(data);
                data.forEach(async (val: any) => {
                  val.team.league_id = param.league;
                  try {
                      const docRef = await addDoc(collection(db, "teams"), val);
                      console.log("Document written with ID: ", docRef.id);
                    } catch (e) {
                      console.error("Error adding document: ", e);
                    }
              });
          })
          .catch(function (error: any) {
          console.log(error);
      })
  );
}

function getPlayerQueriesFunc() {
  const playerQueries = [{id: 730, season: 2021}, {id: 154, season: 2021}, {id: 306, season: 2021}]

  playerQueries.forEach((param: any) => 
      axios({
          method: 'get',
          url: 'https://v3.football.api-sports.io/players',
          headers: {
              'x-rapidapi-key': OTHER_API_KEY,
              'x-rapidapi-host': 'v3.football.api-sports.io'
          },
          params: param
          })
          .then(function (response: any) {
            const data = response.data.response;
            console.log(data);
                data.forEach(async (val: any) => {
                  try {
                      const docRef = await addDoc(collection(db, "players"), val);
                      console.log("Document written with ID: ", docRef.id);
                    } catch (e) {
                      console.error("Error adding document: ", e);
                    }
              });
          })
          .catch(function (error: any) {
          console.log(error);
      })
  );
}

function getPlayerSquadsQueriesFunc() {
  const playerQueries = [{team: 541}, {team: 85}, {team: 40}]

  playerQueries.forEach((param: any) => 
      axios({
          method: 'get',
          url: 'https://v3.football.api-sports.io/players/squads',
          headers: {
              'x-rapidapi-key': OTHER_API_KEY,
              'x-rapidapi-host': 'v3.football.api-sports.io'
          },
          params: param
          })
          .then(function (response: any) {
            const data = response.data.response;
            console.log(data);
                data.forEach(async (val: any) => {
                  try {
                      const docRef = await addDoc(collection(db, "players"), val);
                      console.log("Document written with ID: ", docRef.id);
                    } catch (e) {
                      console.error("Error adding document: ", e);
                    }
              });
          })
          .catch(function (error: any) {
          console.log(error);
      })
  );
}

async function getMatchQueriesFunc() {
  // const matchQueries = [{season: 2021,league: 140, team: 541, last: 1}, {season: 2021,league: 61, team: 85, last: 1}, {season: 2021,league: 39, team: 40, last: 1}];

  const matchQueries = await getModelData("teams");

  matchQueries.forEach(async (param: any) => {
      await axios({
          method: 'get',
          url: 'https://v3.football.api-sports.io/fixtures',
          headers: {
              'x-rapidapi-key': OTHER_API_KEY,
              'x-rapidapi-host': 'v3.football.api-sports.io'
          },
          params: {season: 2021, last: 2, team: param.team.id, league: param.team.league_id}
          })
          .then(async function (response: any) {
            const data = response.data.response;
            console.log(data);
                data.forEach(async (val: any) => {
                  try {
                      const docRef = await addDoc(collection(db, "matches"), val);
                      console.log("Document written with ID: ", docRef.id);
                    } catch (e) {
                      console.error("Error adding document: ", e);
                    }
              });
          })
          .catch(function (error: any) {
          console.log(error);
      });
  }
  );
}

async function getTeamStatsQueriesFunc() {
  
  teams.forEach(async (param: any) => {
    await axios({
      method: 'get',
      url: 'https://v3.football.api-sports.io/teams/statistics',
      headers: {
          'x-rapidapi-key': OTHER_API_KEY,
          'x-rapidapi-host': 'v3.football.api-sports.io'
      },
      params: {season: 2021, league: 39, team: param}
      })
      .then(async function (response: any) {
        const data = response.data.response;
        console.log(data);
          try {
            const docRef = await addDoc(collection(db, "teamstats"), data);
            console.log("Document written with ID: ", docRef.id);
          } catch (e) {
            console.error("Error adding document: ", e);
          }
      })
      .catch(function (error: any) {
      console.log(error);
  });
  } 
  );
}

async function getMatchStatsQueriesFunc() {
  const matchQueries = [{fixture: 710621}, {fixture: 720815}, {fixture: 718439}];

  matchQueries.forEach(async (param: any) => 
      await axios({
          method: 'get',
          url: 'https://v3.football.api-sports.io/fixtures/statistics',
          headers: {
              'x-rapidapi-key': OTHER_API_KEY,
              'x-rapidapi-host': 'v3.football.api-sports.io'
          },
          params: param
          })
          .then(async function (response: any) {
            const data = response.data.response;
            const fullMatch = {fixture: {id: param.fixture}, home: data[0], away: data[1]};
            console.log(data);
            try {
                const docRef = await addDoc(collection(db, "matchstats"), fullMatch);
                console.log("Document written with ID: ", docRef.id);
              } catch (e) {
                console.error("Error adding document: ", e);
              }
          })
          .catch(function (error: any) {
          console.log(error);
      })
  );
}

async function getPlayersInSquadsFunc(index: number) {

  teams.forEach(async (param: any) => {
    await axios({
      method: 'get',
      url: 'https://v3.football.api-sports.io/players',
      headers: {
          'x-rapidapi-key': OTHER_API_KEY,
          'x-rapidapi-host': 'v3.football.api-sports.io'
      },
      params: {team: param, season: 2021, page: index}
      })
      .then(async function (response: any) {
        const data = response.data.response;
            data.forEach(async (val: any) => {
              try {
                  const docRef = await addDoc(collection(db, "players"), val);
                  console.log("Document written with ID: ", docRef.id);
                } catch (e) {
                  console.error("Error adding document: ", e);
                }
          });
      })
      .catch(function (error: any) {
      console.log(error);
      });
  }
  );
}

async function deleteDocs(coll: string) {
  // const querySnapshot = await getDocs(collection(db, coll));
  // querySnapshot.forEach(async (document) => {
  //   await deleteDoc(doc(db, coll, document.id));
  // });
}

async function getModelData(name: string) {
  const data = [] as any;
  const querySnapshot = await getDocs(collection(db, name));
  querySnapshot.forEach((doc) => {
    data.push(doc.data());
  });
  console.log(data);
  return data;
}


const Firebase = { getTeamQueriesFunc, getPlayerQueriesFunc, getMatchQueriesFunc, deleteDocs, getModelData,
  getTeamStatsQueriesFunc, getMatchStatsQueriesFunc, getPlayerSquadsQueriesFunc, getPlayersInSquadsFunc }

export default Firebase;
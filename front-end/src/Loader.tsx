import { Spinner } from "react-bootstrap";

function Loader(props: any) {
    return (
        <div className = "models-loader"><Spinner animation = "border" variant = "dark"/></div>
    );
}

export default Loader;